//Función para igualar el alto de columnas (con padding)
function equalOuterHeight(group) {
    tallest = 0;
    group.each(function () {
        thisHeight = $(this).outerHeight();
        if (thisHeight > tallest) {
            tallest = thisHeight;
        }
    });
    group.outerHeight(tallest);
}

$(document).ready(function () {
    $(".add-gd > a, .gdot").each(function () {
        let $this = $(this);
        let sin = $this.html();
        if (sin.length < 1) return;
        var sout =
            sin.substring(0, sin.length - 1) +
            "<span class=gd>" +
            sin.charAt(sin.length - 1) +
            "</span>";
        $this.html(sout);
    });
    $(".add-bd > a, .bdot").each(function () {
        let $this = $(this);
        let sin = $this.html();
        if (sin.length < 1) return;
        var sout =
            sin.substring(0, sin.length - 1) +
            "<span class=bd>" +
            sin.charAt(sin.length - 1) +
            "</span>";
        $this.html(sout);
    });

    // let vHead = $("header").height();

    // $("main").css("padding-top", vHead + "px");

    //Tabs jQuery UI
    $(function () {
        $(".tabs").tabs({
            show: "fade",
            hide: "fade",
            activate: function (event, ui) {
                newTab = $(ui["newTab"][0]).attr("data-tab");
                console.log($(ui["newTab"][0]).attr("data-tab"));
                if (slider5) {
                    slider5.slick("slickGoTo", 0);
                    slider5.slick("setPosition", 0);
                }
            },
        });
    });

    // Menú responsive
    $(".menu-button").click(function () {
        if ($(".menu-left").css("left") != "0px") {
            $("body").addClass("left-transition");
            $(".menu-button").addClass("close");
            $(".menu-button p span").show();
        } else {
            $("body").removeClass("left-transition");
            $(".menu-button").removeClass("close");
            $(".menu-button p span").hide();
        }
    });

    $("#main-menu .mega-menu").hover(
        function () {
            $("#secondary-menu").addClass("inactive");
        },
        function () {
            $("#secondary-menu").removeClass("inactive");
        }
    );

    $("#carousel-servicios").slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });
    let i = 1;
    $("#work-us")
        .find(".cifra-box .cifra-item")
        .each(function () {
            $(this)
                .find(".number-full")
                .css({
                    "font-size": "1." + i + "5rem",
                });
            i++;
        });
    let m = 0;
    $("#work-us")
        .find(".cifra-box")
        .each(function () {
            $(this).css({
                "margin-top": m * 1.5 + "rem",
            });
            m++;
        });

    $("#soluciones-filter a").click(function (e) {
        e.preventDefault();
        let selectedSol = $(this).data("tax");
        console.log(selectedSol);
        $("#soluciones-filter a").removeClass("is-green").addClass("is-gray");
        $(this).addClass("is-green").removeClass("is-gray");
        $("#soluciones-posts .solucion-cat-container")
            .height(0)
            .removeClass("active");
        let setheightCont = $("#" + selectedSol)
            .find(".icon-boxs-container")
            .height();
        console.log(setheightCont);
        $("#" + selectedSol)
            .css("height", setheightCont + "px")
            .addClass("active");
    });
    $("#soluciones-filter a:first-child")
        .addClass("is-green")
        .removeClass("is-gray");
    $("#soluciones-posts .solucion-cat-container")
        .height(0)
        .removeClass("active");
    let setheightCont = $(
        "#soluciones-posts .solucion-cat-container:first-child"
    )
        .find(".icon-boxs-container")
        .height();
    console.log(setheightCont);
    $("#soluciones-posts .solucion-cat-container:first-child")
        .css("height", setheightCont + "px")
        .addClass("active");

    $("#clientes-carousel").slick({
        rows: 2,
        dots: true,
        arrows: false,
        infinite: true,
        speed: 500,
        slidesPerRow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });
    $("#casos-home-slide").slick({
        adaptiveHeight: true,
        dots: false,
        arrows: false,
        infinite: true,
        speed: 750,
        autoplay: true,
        autoplaySpeed: 3500,
    });
    $("#casos-home-slide .slide-arrows .arrow").each(function () {
        if ($(this).hasClass("prev")) {
            $(this).click(function (e) {
                e.preventDefault();
                $("#casos-home-slide").slick("slickPrev");
            });
        } else if ($(this).hasClass("next")) {
            $(this).click(function (e) {
                e.preventDefault();
                $("#casos-home-slide").slick("slickNext");
            });
        }
    });

    $("#carousel-novedades").slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
    });
    $("#novedades-home .slide-arrows .arrow").each(function () {
        if ($(this).hasClass("prev")) {
            $(this).click(function (e) {
                e.preventDefault();
                $("#carousel-novedades").slick("slickPrev");
            });
        } else if ($(this).hasClass("next")) {
            $(this).click(function (e) {
                e.preventDefault();
                $("#carousel-novedades").slick("slickNext");
            });
        }
    });
});

// Menú responsive
$(document).click(function (e) {
    if (
        !$(e.target).is(
            ".menu-button, .menu-button *, .menu-left, .menu-left *"
        )
    ) {
        $("body").removeClass("left-transition");
        $(".menu-button").removeClass("close");
        $(".menu-button p span").hide();
    }
});
