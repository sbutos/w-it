<?php
/*
Template Name: Resultados de búsqueda
*/
?>

<?php get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Resultados de búsqueda para: %s', 'base' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
				
				the_title();
			?>
			
			<?php
			endwhile;
			// Previous/next page navigation.
		// If no content, include the "No posts found" template.
			else :
			?>
				<?php _e('No hemos encontrado nada :-(', 'base'); ?>
			<?php
		endif;
		?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php get_footer(); ?>