<?php
/* Template Name: Moléculas */
get_header();
?>

	<section id="contenedores">
		<div class="tabs">
			<p class="primary-title">Layout</p>
			<p class="secondary-title">Contenedores</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="block border-radius-s wrap-xl text-center">Contenedor XL (genera un contenedor al 90% del sitio)</div>
				<div class="block border-radius-s wrap-l text-center">Contenedor L (genera un contenedor al 80% del sitio)</div>
				<div class="block border-radius-s wrap-m text-center">Contenedor M (genera un contenedor al 70% del sitio)</div>
				<div class="block border-radius-s wrap-s text-center">Contenedor S (genera un contenedor al 60% del sitio)</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="wrap-xl"&gt;Contenedor XL (genera un contenedor al 90% del sitio)&lt;/div&gt;
&lt;div class="wrap-l"&gt;Contenedor L (genera un contenedor al 80% del sitio)&lt;/div&gt;
&lt;div class="wrap-m"&gt;Contenedor M (genera un contenedor al 70% del sitio)&lt;/div&gt;
&lt;div class="wrap-s"&gt;Contenedor S (genera un contenedor al 60% del sitio)&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_general.scss */

/*------------------------------------------------------*/
/*------------------- CONTENEDORES ---------------------*/
/*------------------------------------------------------*/
.wrap-xl,
.wrap-l,
.wrap-m,
.wrap-s{
  @extend .margin-center;
  @extend .clearfix;
}

.wrap-xl{
  @extend .col-90;
}

.wrap-l{
  @extend .col-80;
}

.wrap-m{
  @extend .col-70;
}

.wrap-s{
  @extend .col-60;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #contenedores -->


	<section id="float">
		<div class="tabs">
			<p class="secondary-title">Floats, margin y padding</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
					<div class="block border-radius-s left">Elemento a la izquierda</div>
					<div class="block border-radius-s right">Elemento a la derecha</div>
					<div class="block border-radius-s margin-center">Elemento al centro</div>
					<div class="block border-radius-s no-margin text-center">Elemento sin margen</div>
					<div class="block border-radius-s no-padding text-center">Elemento sin padding</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="left"&gt;Elemento a la izquierda&lt;/div&gt;
&lt;div class="right"&gt;Elemento a la derecha&lt;/div&gt;
&lt;div class="margin-center"&gt;Elemento al centro&lt;/div&gt;
&lt;div class="no-margin"&gt;Elemento sin margen&lt;/div&gt;
&lt;div class="no-padding"&gt;Elemento sin padding&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_general.scss */

/*------------------------------------------------------*/
/*------------- FLOATS, MARGIN Y PADDING ---------------*/
/*------------------------------------------------------*/
.left{
  float: left;
}

.right{
  float: right;
}

.margin-center{
  margin: 0 auto;
  display: table;
}

.no-margin{
  margin: 0 !important;
}

.no-padding{
  padding: 0 !important;
}
					</code>
				</pre>
			</div><!-- tab-content -->


			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
p{
  @extend .left;
}

p{
  @extend .right;
}

p{
  @extend .margin-center;
}

p{
  @extend .no-margin;
}

p{
  @extend .no-padding;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #float -->



	<section id="position">
		<div class="tabs">
			<p class="secondary-title">Position</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s text-center">
					<p>Existen tres clases para las posiciones: <strong>.relative</strong>, <strong>.absolute</strong> y <strong>.fixed</strong>. De esta manera, y dependiendo de las necesidades del elemento, se le puede asignar una o varias posiciones para diferentes comportamientos.</p>
				</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="relative"&gt;div con posición relative&lt;/div&gt;
&lt;div class="absolute"&gt;div con posición absolute&lt;/div&gt;
&lt;div class="fixed"&gt;div con posición fixed&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_general.scss */

/*------------------------------------------------------*/
/*--------------------- POSICIONES ---------------------*/
/*------------------------------------------------------*/
.relative{
  position: relative;
}

.absolute{
  position: absolute;
}

.fixed{
  position: fixed;
}
					</code>
				</pre>
			</div><!-- tab-content -->


			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
div{
  @extend .relative;
}

div{
  @extend .absolute;
}

div{
  @extend .fixed;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #position -->




	<section id="columns">
		<div class="tabs">
			<p class="secondary-title">Columnas</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s text-center">
					<p>Existe la clase <strong>.col-$i</strong>, donde <strong>$i</strong> se recorre <strong>100 veces</strong>, asignando un ancho por cada vez.</p>
				</div>

				<div class="block border-radius-s col-16">columna al 16%</div>
				<div class="block border-radius-s col-37">columna al 37%</div>
				<div class="block border-radius-s col-50">columna al 50%</div>
				<div class="block border-radius-s col-72">columna al 72%</div>
				<div class="block border-radius-s col-88">columna al 88%</div>
				<div class="block border-radius-s col-100">columna al 100%</div>

			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="col-16"&gt;columna al 16%&lt;/div&gt;
&lt;div class="col-37"&gt;columna al 37%&lt;/div&gt;
&lt;div class="col-50"&gt;columna al 50%&lt;/div&gt;
&lt;div class="col-72"&gt;columna al 72%&lt;/div&gt;
&lt;div class="col-88"&gt;columna al 88%&lt;/div&gt;
&lt;div class="col-100"&gt;columna al 100%&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_general.scss */

/*------------------------------------------------------*/
/*--------------------- COLUMNAS -----------------------*/
/*------------------------------------------------------*/
.col-1{
  width: 1%;
}

/*...y así sucesivamente, hasta llegar al 100*/

.col-100{
  width: 100%;
}
					</code>
				</pre>
			</div><!-- tab-content -->


			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Se realiza un for que recorre del 1 al 100. A cada una le aplica un width del for que recorre, obteniendo así 100 tamaños de columnas. */

@for $i from 1 through 100{
  .col-#{$i}{
    width: $i * 1%;
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #columns -->



	<section id="grids">
		<div class="tabs">
			<p class="secondary-title">Grillas</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s text-center">
					<p>Existe la clase <strong>.grid-column-$i</strong>, donde <strong>$i</strong> se recorre <strong>12 veces</strong>, generando una grilla por este valor.</p>
					<br>
					<p>Además, a este mismo div se le pueden agregar la clases <strong>.gap-s, .gap-m, .gap-l ó .gap-xl</strong>, la cual le añadirá un espaciado entre las columnas de valor <strong>$font*10, $font*20, $font*35 ó $font*50</strong>, como se puede ver en el ejemplo de la grilla de 2 columnas (gap-xl) y 3 columnas (gap-s).</p>
				</div>

				<div class="block border-radius-s grid-column-1">
					<div class="text-center block border-radius-s">1 columna</div>
				</div>

				<div class="block border-radius-s grid-column-2 gap-xl">
					<div class="text-center block border-radius-s">2 columnas, con margen</div>
					<div class="text-center block border-radius-s">2 columnas, con margen</div>
				</div>

				<div class="block border-radius-s grid-column-3 gap-s">
					<div class="text-center block border-radius-s">3 columnas, con margen</div>
					<div class="text-center block border-radius-s">3 columnas, con margen</div>
					<div class="text-center block border-radius-s">3 columnas, con margen</div>
				</div>

				<div class="block border-radius-s grid-column-6">
					<div class="text-center block border-radius-s">6 columnas</div>
					<div class="text-center block border-radius-s">6 columnas</div>
					<div class="text-center block border-radius-s">6 columnas</div>
					<div class="text-center block border-radius-s">6 columnas</div>
					<div class="text-center block border-radius-s">6 columnas</div>
					<div class="text-center block border-radius-s">6 columnas</div>
				</div>

				<div class="block border-radius-s grid-column-12">
					<div class="text-center block border-radius-s">12 columnas</div>
					<div class="text-center block border-radius-s">12 columnas</div>
					<div class="text-center block border-radius-s">12 columnas</div>
					<div class="text-center block border-radius-s">12 columnas</div>
					<div class="text-center block border-radius-s">12 columnas</div>
					<div class="text-center block border-radius-s">12 columnas</div>
					<div class="text-center block border-radius-s">12 columnas</div>
					<div class="text-center block border-radius-s">12 columnas</div>
					<div class="text-center block border-radius-s">12 columnas</div>
					<div class="text-center block border-radius-s">12 columnas</div>
					<div class="text-center block border-radius-s">12 columnas</div>
					<div class="text-center block border-radius-s">12 columnas</div>
				</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="block border-radius-s grid-column-1"&gt;
  &lt;div&gt;1 columna&lt;/div&gt;
&lt;/div&gt;

&lt;div class="block border-radius-s grid-column-2 gap-s"&gt;
  &lt;div&gt;2 columnas, con margen&lt;/div&gt;
  &lt;div&gt;2 columnas, con margen&lt;/div&gt;
&lt;/div&gt;

&lt;div class="block border-radius-s grid-column-3 gap-xl"&gt;
  &lt;div&gt;3 columnas, con margen&lt;/div&gt;
  &lt;div&gt;3 columnas, con margen&lt;/div&gt;
  &lt;div&gt;3 columnas, con margen&lt;/div&gt;
&lt;/div&gt;

&lt;div class="block border-radius-s grid-column-6"&gt;
  &lt;div&gt;6 columnas&lt;/div&gt;
  &lt;div&gt;6 columnas&lt;/div&gt;
  &lt;div&gt;6 columnas&lt;/div&gt;
  &lt;div&gt;6 columnas&lt;/div&gt;
  &lt;div&gt;6 columnas&lt;/div&gt;
  &lt;div&gt;6 columnas&lt;/div&gt;
&lt;/div&gt;

&lt;div class="block border-radius-s grid-column-12"&gt;
  &lt;div&gt;12 columnas&lt;/div&gt;
  &lt;div&gt;12 columnas&lt;/div&gt;
  &lt;div&gt;12 columnas&lt;/div&gt;
  &lt;div&gt;12 columnas&lt;/div&gt;
  &lt;div&gt;12 columnas&lt;/div&gt;
  &lt;div&gt;12 columnas&lt;/div&gt;
  &lt;div&gt;12 columnas&lt;/div&gt;
  &lt;div&gt;12 columnas&lt;/div&gt;
  &lt;div&gt;12 columnas&lt;/div&gt;
  &lt;div&gt;12 columnas&lt;/div&gt;
  &lt;div&gt;12 columnas&lt;/div&gt;
  &lt;div&gt;12 columnas&lt;/div&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_general.scss */
/* Se realiza un for que recorre del 1 al 12. A cada una le aplica un grid-template-columns: repeat, asignándole el valor del for. Además, incorporando la clase .with-gap al mismo div, se puede asignar un espaciado entre las columnas de valor $font * 50 */
/*------------------------------------------------------*/
/*---------------------- GRILLAS -----------------------*/
/*------------------------------------------------------*/

@for $i from 1 through 12{
  .grid-column-#{$i}{
    display: grid;
    grid-template-columns: repeat($i, 1fr);
    &.with-gap{
      grid-gap: $font * 50;
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #grids -->




	<section id="text-columns">
		<div class="tabs">
			<p class="secondary-title">Columnas de texto</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s text-center">
					<p>Existe la clase <strong>.text-columns-$i</strong>, donde <strong>$i</strong> se recorre <strong>4 veces</strong> (desde el 2 al 5), asignando una cantidad de columnas para separar el texto.</p>
				</div>

				<div class="block border-radius-s text-columns-2">
					<p>Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.</p>
					<p>Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</p>
				</div>

				<div class="block border-radius-s text-columns-3">
					<p>Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.</p>
					<p>Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</p>
				</div>

				<div class="block border-radius-s text-columns-4">
					<p>Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.</p>
					<p>Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</p>
				</div>

				<div class="block border-radius-s text-columns-5">
					<p>Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.</p>
					<p>Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</p>
				</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="text-columns-2"&gt;
  &lt;p&gt;Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.&lt;/p&gt;
  &lt;p&gt;Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.&lt;/p&gt;
&lt;/div&gt;

&lt;div class="text-columns-3"&gt;
  &lt;p&gt;Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.&lt;/p&gt;
  &lt;p&gt;Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.&lt;/p&gt;
&lt;/div&gt;

&lt;div class="text-columns-4"&gt;
  &lt;p&gt;Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.&lt;/p&gt;
  &lt;p&gt;Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.&lt;/p&gt;
&lt;/div&gt;

&lt;div class="text-columns-5"&gt;
  &lt;p&gt;Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.&lt;/p&gt;
  &lt;p&gt;Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.&lt;/p&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_general.scss */

/*------------------------------------------------------*/
/*----------------- COLUMNAS DE TEXTO ------------------*/
/*------------------------------------------------------*/
.text-columns-2{
  column-count: 2;
  -webkit-column-count: 2;
  -moz-column-count: 2;
  column-gap: 50px;
  -webkit-column-gap: 50px;
  -moz-column-gap: 50px;
}

/*...y así sucesivamente, hasta llegar al 5*/

.text-columns-5{
  column-count: 5;
  -webkit-column-count: 5;
  -moz-column-count: 5;
  column-gap: 50px;
  -webkit-column-gap: 50px;
  -moz-column-gap: 50px;
}
					</code>
				</pre>
			</div><!-- tab-content -->


			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Se realiza un for que recorre del 2 al 5. A cada una le aplica un column-count del for que recorre, obteniendo así 4 tamaños de columnas. Además, se obtiene el ancho de la columna multiplicando * 50 el valor de $font */

@for $i from 2 through 5{
  .text-columns-#{$i}{
    column-count: $i;
    -webkit-column-count: $i;
    -moz-column-count: $i;
    column-gap: $font * 50;
    -webkit-column-gap: $font * 50;
    -moz-column-gap: $font * 50;
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #text-columns -->




	<section id="flex-same-children">
		<div class="tabs">
			<p class="secondary-title">Flex (mismo ancho para los hijos)</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s clearfix">
					<p class="text-center">Para que los hijos de un elemento de tipo <strong>.flex</strong> tengan el mismo ancho, simplemente debe incorporarse la clase <strong>.flex-same-children</strong>.</p>
				</div>

				<ul class="flex flex-same-children block border-radius-s">
					<li class="text-center">Elemento 1</li>
					<li class="text-center">Elemento 2</li>
					<li class="text-center">Elemento 3</li>
					<li class="text-center">Elemento 4</li>
				</ul>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;ul class="flex flex-same-children"&gt;
  &lt;li&gt;Elemento 1&lt;/li&gt;
  &lt;li&gt;Elemento 2&lt;/li&gt;
  &lt;li&gt;Elemento 3&lt;/li&gt;
  &lt;li&gt;Elemento 4&lt;/li&gt;
&lt;/ul&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_flex.scss */

/*------------------------------------------------------*/
/*------------------------ FLEX ------------------------*/
/*------------------------------------------------------*/
.flex{
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
}

.flex-same-children > *{
  flex-grow: 1;
  flex-basis: 0;
  flex: 1 1 0;
  width: 0;
}
					</code>
				</pre>
			</div><!-- tab-content -->


			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
.flex{
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  &.flex-same-children > *{
    flex-grow: 1;
    flex-basis: 0;
    flex: 1 1 0;
    width: 0;
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #flex-same-children -->




	<section id="flex-justify-content">
		<div class="tabs">
			<p class="secondary-title">Flex (Justificar contenido)</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s clearfix">
					<p>Para que un div tenga la propiedad <strong>display: flex;</strong> simplemente debe incorporarse la clase <strong>.flex</strong></p>
					<br>
					<p>En cuanto a la "justificación" de los ítems, debe asignársele alguna de estas clases para:</p>
					<ol>
						<li><strong>.justify-center (el del ejemplo):</strong> Distribuir los hijos al centro</li>
						<li><strong>.justify-between:</strong> Distribuirlos a lo largo del contenedor</li>
						<li><strong>.justify-evenly:</strong> Distribuirlos de manera uniforme y tengan el mismo espacio alrededor</li>
						<li><strong>.justify-start:</strong> Distribuirlos desde la izquierda</li>
						<li><strong>.justify-end:</strong> Distribuirlos desde la derecha</li>
						<li><strong>.justify-around:</strong> Distribuirlos de manera uniforme. Tienen un espacio de tamaño medio en cada extremo</li>
					</ol>
				</div>

				<ul class="flex justify-center block border-radius-s">
					<li>Elemento 1</li>
					<li>Elemento 2</li>
					<li>Elemento 3</li>
					<li>Elemento 4</li>
				</ul>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;ul class="flex justify-center"&gt;
  &lt;li&gt;Elemento 1&lt;/li&gt;
  &lt;li&gt;Elemento 2&lt;/li&gt;
  &lt;li&gt;Elemento 3&lt;/li&gt;
  &lt;li&gt;Elemento 4&lt;/li&gt;
&lt;/ul&gt;
					</code>
				</pre>
			</div><!-- tab-content -->


			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación del @mixin: components/merlin/scss/_mixins.scss */
@mixin just($position){
  justify-content: $position;
}

/* Ubicación: components/merlin/scss/_flex.scss */
.justify-center{
    @include just(center);
}
.justify-between{
    @include just(space-between);
}
.justify-evenly{
    @include just(space-evenly);
}
.justify-start{
    @include just(flex-start);
}
.justify-end{
    @include just(flex-end);
}
.justify-around{
    @include just(space-around);
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #flex-justify-content -->



	<section id="flex-align">
		<div class="tabs">
			<p class="secondary-title">Flex (alinear)</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s clearfix">
					<p>Para que un div tenga la propiedad <strong>display: flex;</strong> simplemente debe incorporarse la clase <strong>.flex</strong></p>
					<br>
					<p>Para alinear los ítems, debe asignársele alguna de estas clases para:</p>
					<ol>
						<li><strong>.align-center (el del ejemplo):</strong> Alinear los hijos al centro</li>
						<li><strong>.align-between:</strong> Distribuirlos a lo largo del contenedor</li>
						<li><strong>.align-start:</strong> Distribuirlos desde el top del contenedor</li>
						<li><strong>.align-end:</strong> Distribuirlos desde el bottom del contenedor</li>
						<li><strong>.align-around:</strong> Distribuirlos con espacio antes, entre y después de las líneas</li>
					</ol>
				</div>

				<ul class="block border-radius-s flex flex-same-children align-center">
					<li class="block border-radius-s">Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.</li>
					<li class="block border-radius-s">Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</li>
					<li class="block border-radius-s">Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. - ¿Qué me ha ocurrido? No estaba soñando.</li>
				</ul>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;ul class="flex flex-same-children align-center"&gt;
  &lt;li&gt;Elemento 1&lt;/li&gt;
  &lt;li&gt;Elemento 2&lt;/li&gt;
  &lt;li&gt;Elemento 3&lt;/li&gt;
  &lt;li&gt;Elemento 4&lt;/li&gt;
&lt;/ul&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación del @mixin: components/merlin/scss/_mixins.scss */
@mixin align($position) {
  align-items: $position;
}

/* Ubicación: components/merlin/scss/_flex.scss */
.align-center{
    @include align(center);
}
.align-between{
    @include align(space-between);
}
.align-start{
    @include align(flex-start);
}
.align-end{
    @include align(flex-end);
}
.align-around{
    @include align(space-around);
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #flex-align -->


	<section id="modal">
		<div class="tabs">
			<p class="secondary-title">Modal</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s clearfix">
					Si el modal incluye un video que se quiere reproducir de manera automática, hay que incluir en el trigger, el tag <strong>data-video-url="id_video_yt"</strong>
					<br>
					Este módulo requiere de la llamada en el header del script externo <a href="https://www.youtube.com/iframe_api">iframe_api</a>, además del <a href="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/modal.js">modal.js</a> incluído en Merlín.
				</div>

				<ul class="grid-column-3 gap-l">
					<li class="block border-radius-s text-center">
						<div data-id="modal-numero-uno" class="modal-trigger">Modal con texto</div>
					</li>
					<li class="block border-radius-s text-center">
						<div data-id="modal-numero-dos" data-video-url="_OkvnfzL93o" class="modal-trigger">Modal con video uno</div>
					</li>

					<li class="block border-radius-s text-center">
						<div data-id="modal-numero-dos" data-video-url="llqDrq00E28" class="modal-trigger">Modal con video dos</div>
					</li>
				</ul>


				<div data-id="modal-numero-uno" class="modal">
					<i class="close icon-equis"></i>
					<div class="content-modal contenido wp-content">
						<p class="title">Modal número uno</p>
						<p>
							Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>
						</p>
					</div><!-- content-modal -->
					<div class="modal-background"></div>
				</div><!-- modal -->

				<div data-id="modal-numero-dos" class="modal">
					<i class="close icon-equis"></i>
					<div class="content-modal contenido wp-content">
						<div class="iframeVideo relative">
							<div id="player"></div>
						</div>
					</div><!-- content -->
					<div class="modal-background"></div>
				</div><!-- modal -->

				<div data-id="modal-numero-tres" class="modal">
					<i class="close icon-equis"></i>
					<div class="content-modal contenido wp-content">
						<div class="iframeVideo relative">
							<div id="player"></div>
						</div>
					</div><!-- content -->
					<div class="modal-background"></div>
				</div><!-- modal -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #flex-align -->
<?php get_footer(); ?>
