<!doctype html>

<html lang="es" <?php language_attributes() ?>>

<head profile="http://gmpg.org/xfn/11">
    <title><?php echo esc_html( get_bloginfo('name'), 1 ); wp_title( '|', true, 'left' ); ?></title>
    <meta charset="<?php bloginfo('charset') ?>">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url') ?>" />
    <?php wp_head() ?>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous" type="text/javascript">
    </script>

    <!-- jQuery Tabs -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>

    <!-- API YouTube -->
    <script src="https://www.youtube.com/iframe_api"></script>

    <!-- Scripts utilizados en el tema -->
    <link rel="stylesheet" type="text/css"
        href="<?php echo get_stylesheet_directory_uri(); ?>/components/custom/js/highlight/androidstudio.css" />
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/components/custom/js/highlight/highlight.pack.js"
        type="text/javascript"></script>
    <script>
    hljs.initHighlightingOnLoad();
    </script>

    <!-- Scripts de Merlín -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/simpleParallax.min.js"
        type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/scripts.js" type="text/javascript">
    </script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/modal.js" type="text/javascript">
    </script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/toggle.js" type="text/javascript">
    </script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/header.js" type="text/javascript">
    </script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/animacion-anclas.js"
        type="text/javascript"></script>

    <!-- Slick -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/slick/slick.min.js"
        type="text/javascript"></script>
    <link rel="stylesheet" type="text/css"
        href="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/slick/slick.css" />

    <!-- AOS -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/aos/aos.js" type="text/javascript">
    </script>
    <link rel="stylesheet" type="text/css"
        href="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/aos/aos.css" />

    <!-- Lightgallery -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/lightgallery/lightgallery.js"
        type="text/javascript"></script>
    <link rel="stylesheet" type="text/css"
        href="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/lightgallery/lightgallery.css" />

    <!-- Scripts de organismos -->
    <!-- Sliders -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/organisms/sliders/01/js/slider-1.js"
        type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/organisms/sliders/02/js/slider-2.js"
        type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/organisms/sliders/03/js/slider-3.js"
        type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/organisms/sliders/04/js/slider-4.js"
        type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/organisms/sliders/05/js/slider-5.js"
        type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/organisms/sliders/06/js/slider-6.js"
        type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/organisms/sliders/07/js/slider-7.js"
        type="text/javascript"></script>

    <!-- Blocks -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/count-up.js"
        type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/in-view.js"
        type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/block-3.js"
        type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/05/js/block-5.js"
        type="text/javascript"></script>

    <!-- Responsive -->
    <meta name="viewport"
        content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
</head>

<body>
    <header class="col-100 left clearfix">
        <div class="wrap-xl">
            <div id="main-header" class="flex align-center justify-between">
                <div class="brand-area">
                    <a href="<?php echo site_url('/'); ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-wit.svg" alt="W-It">
                    </a>
                    <div id="hamburger" class="relative clearfix menu-trigger menu-right-button transition-slower">
                        <span class="line-1"></span>
                        <span class="line-2"></span>
                        <span class="line-3"></span>
                    </div>
                </div>
                <div class="menu-area">
                    <ul id="main-menu">
                        <?php wp_nav_menu(array('items_wrap' => '%3$s', 'theme_location'=>'Menu principal', 'container_class' => false, 'container' => false));?>
                    </ul>
                    <ul id="secondary-menu">
                        <?php wp_nav_menu(array('items_wrap' => '%3$s', 'theme_location'=>'Menu secundario', 'container_class' => false, 'container' => false));?>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <div class="menu-responsive">
        <div class="menu-container">
            <ul class="menu bold uppercase">
                <?php wp_nav_menu(array('items_wrap' => '%3$s', 'theme_location'=>'Menu principal', 'container_class' => false, 'container' => false));?>
            </ul>
        </div><!-- menu-container -->
    </div><!--	menu-right -->

    <main>