<?php get_header() ?>

	<?php if ( have_rows( 'contenido_flexible' ) ): ?>
		<?php while ( have_rows( 'contenido_flexible' ) ) : the_row(); ?>
			<?php if ( get_row_layout() == 'slider_1' ) : ?>
				<?php if ( have_rows( 'slider_1' ) ) : ?>
					<section class="slider-1">
						<?php while ( have_rows( 'slider_1' ) ) : the_row(); ?>
							<?php
							$slider_1_tipo = get_sub_field( 'slider_1_tipo' );
							$slider_1_video_subir = get_sub_field( 'slider_1_video_subir' );
							$slider_1_youtube = get_sub_field( 'slider_1_youtube' );
							$slider_1_vimeo = get_sub_field( 'slider_1_vimeo' );
							$slider_1_imagen = get_sub_field( 'slider_1_imagen' );
							$slider_1_pre_titulo = get_sub_field( 'slider_1_pre_titulo' );
							$slider_1_titulo = get_sub_field( 'slider_1_titulo' );
							$slider_1_descripcion = get_sub_field( 'slider_1_descripcion' );
							$slider_1_link = get_sub_field( 'slider_1_link' );
							if( $slider_1_link ){
								$slider_1_link_url = $slider_1_link['url'];
								$slider_1_link_title = $slider_1_link['title'];
								$slider_1_link_target = $slider_1_link['target'] ? $slider_1_link['target'] : '_self';
							}
							?>
							<div class="slide <?php if($slider_1_tipo == 'subir-video'){ echo 'video'; }elseif($slider_1_tipo == 'youtube'){ echo 'youtube'; }elseif($slider_1_tipo == 'vimeo'){ echo 'vimeo'; } ?>">
								<div class="content col-50 relative z-index-2">
									<div class="slider-numbers-container relative clearfix">
										<div class="slider-numbers relative"></div>
										<div class="timer relative">
											<figure class="counter col-1 transform-time"></figure>
										</div>
									</div><!-- slider-numbers-container -->

									<?php if($slider_1_pre_titulo){ ?>
										<p class="pre-title" data-aos="fade-right"><?php echo $slider_1_pre_titulo; ?></p>
									<?php } ?>

									<?php if($slider_1_titulo){ ?>
										<p class="title" data-aos="fade-right" data-aos-delay="300"><?php echo $slider_1_titulo; ?></p>
									<?php } ?>

									<?php if($slider_1_descripcion){ ?>
										<div class="description" data-aos="fade-right" data-aos-delay="500">
											<?php echo $slider_1_descripcion; ?>
										</div>
									<?php } ?>

									<?php if($slider_1_link){ ?>
										<a href="<?php echo $slider_1_link_url; ?>" class="button" target="<?php echo $slider_1_link_target; ?>" data-aos="fade-right" data-aos-delay="650" data-aos-offset="-250">
											<?php echo $slider_1_link_title; ?>
										</a>
									<?php } ?>
								</div><!-- content -->
								<div class="veil"></div>

								<?php if($slider_1_tipo == 'youtube' && $slider_1_youtube){ ?>
									<iframe class="embed-player" src="https://www.youtube.com/embed/<?php echo $slider_1_youtube; ?>?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=<?php echo $slider_1_youtube; ?>&start=0&mute=1&autoplay=0" frameborder="0" allowfullscreen></iframe>
									<div class="photo cover zoom" style="background-image:url('<?php echo $slider_1_imagen; ?>')"></div>
								<?php } ?>

								<?php if($slider_1_tipo == 'vimeo' && $slider_1_vimeo){ ?>
									<iframe class="embed-player" src="https://player.vimeo.com/video/<?php echo $slider_1_vimeo; ?>?api=1&byline=0&portrait=0&title=0&background=1&mute=1&loop=1&autoplay=0&id=<?php echo $slider_1_vimeo; ?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
									<div class="photo cover zoom" style="background-image:url('<?php echo $slider_1_imagen; ?>')"></div>
								<?php } ?>

								<?php if($slider_1_tipo == 'subir-video' && $slider_1_video_subir){ ?>
									<video controls loop playsinline muted preload="none">
										<source src="<?php echo $slider_1_video_subir; ?>" type="video/mp4" autostart="false">
									</video>
									<div class="photo cover zoom" style="background-image:url('<?php echo $slider_1_imagen; ?>')"></div>
								<?php } ?>

								<?php if($slider_1_tipo == 'fotografia' && $slider_1_imagen){ ?>
									<div class="photo cover zoom" style="background-image:url('<?php echo $slider_1_imagen; ?>')"></div>
								<?php } ?>
							</div><!-- slide -->
						<?php endwhile; ?>
					</section><!-- slider-1 -->
				<?php endif; //slider_1 ?>

			<?php elseif ( get_row_layout() == 'slider_2' ) : ?>
				<?php if ( have_rows( 'slider_2' ) ) : ?>
					<section class="slider-2 col-100 left clearfix relative">
						<?php
						while ( have_rows( 'slider_2' ) ) : the_row();
						$slider_2_imagen = get_sub_field( 'slider_2_imagen' );
						$slider_2_disposicion = get_sub_field( 'slider_2_disposicion' );
						?>
							<div class="slide <?php echo $slider_2_disposicion; ?>" style="background-image:url('<?php echo $slider_2_imagen; ?>')"></div>
						<?php endwhile; ?>
					</section><!-- slider-2 -->
				<?php endif; // slider_2 ?>

			<?php elseif ( get_row_layout() == 'slider_3' ) : ?>
				<?php if ( have_rows( 'slider_3' ) ) : ?>
					<section class="slider-3-container col-100 left clearfix relative">
						<div class="slider-3 col-100 left clearfix relative">
							<?php while ( have_rows( 'slider_3' ) ) : the_row(); ?>
								<?php
								$slider_3_imagen = get_sub_field( 'slider_3_imagen' );
								$slider_3_pre_titulo = get_sub_field( 'slider_3_pre_titulo' );
								$slider_3_ano = get_sub_field( 'slider_3_ano' );
								$slider_3_titulo = get_sub_field( 'slider_3_titulo' );
								$slider_3_descripcion = get_sub_field( 'slider_3_descripcion' );
								$slider_3_link = get_sub_field( 'slider_3_link' );
								if( $slider_3_link ){
									$slider_3_link_url = $slider_3_link['url'];
									$slider_3_link_title = $slider_3_link['title'];
									$slider_3_link_target = $slider_3_link['target'] ? $slider_3_link['target'] : '_self';
								}
								?>
								<div class="slide col-100 left clearfix relative overflow-hidden">
									<div class="content">
										<?php if($slider_3_pre_titulo){ ?>
											<p class="main-title"><?php echo $slider_3_pre_titulo; ?></p>
										<?php } ?>

										<?php if($slider_3_ano){ ?>
											<p class="year"><?php echo $slider_3_ano; ?></p>
										<?php } ?>

										<?php if($slider_3_titulo){ ?>
											<p class="title"><?php echo $slider_3_titulo; ?></p>
										<?php } ?>

										<?php if($slider_3_descripcion){ ?>
											<div class="description">
												<?php echo $slider_3_descripcion; ?>
											</div>
										<?php } ?>

										<?php if($slider_3_link){ ?>
											<a href="<?php echo $slider_3_link_url; ?>" class="button" target="<?php echo $slider_3_link_target; ?>">
												<?php echo $slider_3_link_title; ?>
											</a>
										<?php } ?>
									</div><!-- content -->
									<div class="veil"></div>
									<?php if($slider_3_imagen){ ?>
										<div class="photo cover zoom" style="background-image:url('<?php echo $slider_3_imagen; ?>')"></div>
									<?php } ?>
								</div><!-- slide -->
							<?php endwhile; ?>
						</div><!-- slider-3 -->

						<ul class="slider-nav-3">
							<?php
							while ( have_rows( 'slider_3' ) ) : the_row();
							$slider_3_ano = get_sub_field( 'slider_3_ano' );
							?>
								<li><?php echo $slider_3_ano; ?></li>
							<?php endwhile; ?>
						</ul>
					</section><!-- slider-3-container -->
				<?php endif; //slider_3 ?>

			<?php elseif ( get_row_layout() == 'slider_4' ) : ?>
				<?php if ( have_rows( 'slider_4' ) ) : ?>
						<section class="slider-4-container col-100 left clearfix relative">
							<ul class="slider-nav-4">
								<?php
								while ( have_rows( 'slider_4' ) ) : the_row();
								$slider_4_imagen = get_sub_field( 'slider_4_imagen' );
								$slider_4_pre_titulo = get_sub_field( 'slider_4_pre_titulo' );
								$slider_4_titulo = get_sub_field( 'slider_4_titulo' );
								?>
									<li>
										<?php if($slider_4_imagen){ ?>
											<div class="photography cover" style="background-image:url('<?php echo $slider_4_imagen; ?>')"></div>
										<?php } ?>

										<?php if($slider_4_pre_titulo || $slider_4_titulo){ ?>
											<div class="content">
												<?php if($slider_4_pre_titulo){ ?>
													<p class="pre-title"><?php echo $slider_4_pre_titulo; ?></p>
												<?php } ?>

												<?php if($slider_4_titulo){ ?>
													<p class="title"><?php echo $slider_4_titulo; ?></p>
												<?php } ?>
											</div>
										<?php } ?>
									</li>
								<?php endwhile; ?>
							</ul>

							<div class="slider-4 col-100 left clearfix relative">
								<?php while ( have_rows( 'slider_4' ) ) : the_row(); ?>
									<?php
									$slider_4_imagen = get_sub_field( 'slider_4_imagen' );
									$slider_4_pre_titulo = get_sub_field( 'slider_4_pre_titulo' );
									$slider_4_titulo = get_sub_field( 'slider_4_titulo' );
									$slider_4_descripcion = get_sub_field( 'slider_4_descripcion' );
									$slider_4_link = get_sub_field( 'slider_4_link' );
									if( $slider_4_link ){
										$slider_4_link_url = $slider_4_link['url'];
										$slider_4_link_title = $slider_4_link['title'];
										$slider_4_link_target = $slider_4_link['target'] ? $slider_4_link['target'] : '_self';
									}
									?>
									<div class="slide col-100 left clearfix relative overflow-hidden">
										<div class="content">
											<?php if($slider_4_pre_titulo){ ?>
												<p class="pre-title"><?php echo $slider_4_pre_titulo; ?></p>
											<?php } ?>

											<?php if($slider_4_titulo){ ?>
												<p class="title"><?php echo $slider_4_titulo; ?></p>
											<?php } ?>

											<?php if($slider_4_descripcion){ ?>
												<div class="description">
													<?php echo $slider_4_descripcion; ?>
												</div>
											<?php } ?>

											<?php if($slider_4_link){ ?>
												<a href="<?php echo $slider_4_link_url; ?>" class="button" target="<?php echo $slider_4_link_target; ?>">
													<?php echo $slider_4_link_title; ?>
												</a>
											<?php } ?>
										</div><!-- content -->
										<div class="veil"></div>
										<?php if($slider_4_imagen){ ?>
											<div class="photo cover zoom" style="background-image:url('<?php echo $slider_4_imagen; ?>')"></div>
										<?php } ?>
									</div><!-- slide -->
								<?php endwhile; ?>
						</div><!-- slider-4 -->
					</section><!-- slider-4-container -->
				<?php endif; //slider_4 ?>


			<?php elseif ( get_row_layout() == 'slider_5' ) : ?>
				<?php if ( have_rows( 'slider_5' ) ) : ?>
						<section class="slider-5-container tabs col-100 left clearfix relative">
							<ul class="slider-nav-5 grid-column-2">
								<?php
								$i = 0;
								while ( have_rows( 'slider_5' ) ) : the_row();
								$slider_5_imagen_principal = get_sub_field( 'slider_5_imagen_principal' );
								$slider_5_pre_titulo_principal = get_sub_field( 'slider_5_pre_titulo_principal' );
								$slider_5_titulo_principal = get_sub_field( 'slider_5_titulo_principal' );
								?>
								<li data-tab="<?php echo $i; ?>">
									<a href="#<?php echo sanitize_title($slider_5_titulo_principal); ?>">
										<?php if($slider_5_imagen_principal){ ?>
											<div class="photography cover" style="background-image:url('<?php echo $slider_5_imagen_principal; ?>')"></div>
										<?php } ?>

										<?php if($slider_5_pre_titulo_principal || $slider_5_titulo_principal){ ?>
											<div class="content">
												<?php if($slider_5_pre_titulo_principal){ ?>
													<p class="pre-title"><?php echo $slider_5_pre_titulo_principal; ?></p>
												<?php } ?>

												<?php if($slider_5_titulo_principal){ ?>
													<p class="title"><?php echo $slider_5_titulo_principal; ?></p>
												<?php } ?>
											</div>
										<?php } ?>
									</a>
								</li>
								<?php $i++; endwhile; ?>
							</ul>

							<?php
							while ( have_rows( 'slider_5' ) ) : the_row();
							$slider_5_titulo_principal = get_sub_field( 'slider_5_titulo_principal' );
							if ( have_rows( 'slider_5_sliders' ) ) : ?>
								<div id="<?php echo sanitize_title($slider_5_titulo_principal); ?>" class="slider-5 col-100 left clearfix relative">
									<?php while ( have_rows( 'slider_5_sliders' ) ) : the_row(); ?>
									<?php
									$slider_5_imagen = get_sub_field( 'slider_5_imagen' );
									$slider_5_pre_titulo = get_sub_field( 'slider_5_pre_titulo' );
									$slider_5_titulo = get_sub_field( 'slider_5_titulo' );
									$slider_5_descripcion = get_sub_field( 'slider_5_descripcion' );
									$slider_5_link = get_sub_field( 'slider_5_link' );
									if( $slider_5_link ){
										$slider_5_link_url = $slider_5_link['url'];
										$slider_5_link_title = $slider_5_link['title'];
										$slider_5_link_target = $slider_5_link['target'] ? $slider_5_link['target'] : '_self';
									}
									?>
									<div id="<?php echo sanitize_title($slider_5_titulo); ?>" class="slide col-100 left clearfix relative overflow-hidden">
										<div class="content">
											<?php if($slider_5_pre_titulo){ ?>
												<p class="pre-title"><?php echo $slider_5_pre_titulo; ?></p>
											<?php } ?>

											<?php if($slider_5_titulo){ ?>
												<p class="title"><?php echo $slider_5_titulo; ?></p>
											<?php } ?>

											<?php if($slider_5_descripcion){ ?>
												<div class="description">
													<?php echo $slider_5_descripcion; ?>
												</div>
											<?php } ?>

											<?php if($slider_5_link){ ?>
												<a href="<?php echo $slider_5_link_url; ?>" class="button" target="<?php echo $slider_5_link_target; ?>">
													<?php echo $slider_5_link_title; ?>
												</a>
											<?php } ?>
										</div><!-- content -->
										<div class="veil"></div>
										<?php if($slider_5_imagen){ ?>
											<div class="photo cover zoom" style="background-image:url('<?php echo $slider_5_imagen; ?>')"></div>
										<?php } ?>
									</div><!-- slide -->
								<?php endwhile; ?>
							</div><!-- slider-5 -->
						<?php
							endif;
						endwhile; ?>
					</section><!-- slider-5-container -->
				<?php endif; //slider_5 ?>


			<?php elseif ( have_rows( 'slider_6' ) ) : ?>
				<?php if ( have_rows( 'slider_6' ) ) : ?>
					<section class="slider-6-container">
						<div class="slider-6">
							<?php while ( have_rows( 'slider_6' ) ) : the_row(); ?>
								<?php
								$slider_6_tipo = get_sub_field( 'slider_6_tipo' );
								$slider_6_video_subir = get_sub_field( 'slider_6_video_subir' );
								$slider_6_youtube = get_sub_field( 'slider_6_youtube' );
								$slider_6_vimeo = get_sub_field( 'slider_6_vimeo' );
								$slider_6_imagen = get_sub_field( 'slider_6_imagen' );
								?>
								<div class="slide <?php if($slider_6_tipo == 'subir-video'){ echo 'video'; }elseif($slider_6_tipo == 'youtube'){ echo 'youtube'; }elseif($slider_6_tipo == 'vimeo'){ echo 'vimeo'; } ?>">
									<?php if($slider_6_tipo == 'youtube' && $slider_6_youtube){ ?>
										<iframe class="embed-player" src="https://www.youtube.com/embed/<?php echo $slider_6_youtube; ?>?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=<?php echo $slider_6_youtube; ?>&start=0&mute=1&autoplay=0" frameborder="0" allowfullscreen></iframe>
									<?php } ?>

									<?php if($slider_6_tipo == 'vimeo' && $slider_6_vimeo){ ?>
										<iframe class="embed-player" src="https://player.vimeo.com/video/<?php echo $slider_6_vimeo; ?>?api=1&byline=0&portrait=0&title=0&background=1&mute=1&loop=1&autoplay=0&id=<?php echo $slider_6_vimeo; ?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
									<?php } ?>

									<?php if($slider_6_tipo == 'subir-video' && $slider_6_video_subir){ ?>
										<video controls loop playsinline muted preload="none">
											<source src="<?php echo $slider_6_video_subir; ?>" type="video/mp4" autostart="false">
										</video>
									<?php } ?>

									<?php if($slider_6_tipo == 'fotografia' && $slider_6_imagen){ ?>
										<div class="photo cover zoom" style="background-image:url('<?php echo $slider_6_imagen; ?>')"></div>
									<?php } ?>
								</div><!-- slide -->
							<?php endwhile; ?>
						</div><!-- slider-6 -->

						<div class="slider-6-nav">
							<?php while ( have_rows( 'slider_6' ) ) : the_row(); ?>
								<?php
								$slider_6_tipo = get_sub_field( 'slider_6_tipo' );
								$slider_6_video_subir = get_sub_field( 'slider_6_video_subir' );
								$slider_6_youtube = get_sub_field( 'slider_6_youtube' );
								$slider_6_vimeo = get_sub_field( 'slider_6_vimeo' );
								$slider_6_imagen = get_sub_field( 'slider_6_imagen' );
								?>
								<div class="slide">
									<?php if($slider_6_tipo == 'youtube' && $slider_6_youtube){ ?>
										<div class="veil"></div>
										<iframe src="https://www.youtube.com/embed/<?php echo $slider_6_youtube; ?>?enablejsapi=1&controls=1&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=<?php echo $slider_6_youtube; ?>&start=0&mute=1&autoplay=0" frameborder="0" allowfullscreen></iframe>
									<?php } ?>

									<?php if($slider_6_tipo == 'vimeo' && $slider_6_vimeo){ ?>
										<div class="veil"></div>
										<iframe src="https://player.vimeo.com/video/<?php echo $slider_6_vimeo; ?>?api=1&byline=0&portrait=0&title=0&background=1&mute=1&loop=1&autoplay=0&id=<?php echo $slider_6_vimeo; ?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
									<?php } ?>

									<?php if($slider_6_tipo == 'subir-video' && $slider_6_video_subir){ ?>
										<video muted controls="false">
											<source src="<?php echo $slider_6_video_subir; ?>" type="video/mp4" autostart="false">
										</video>
										<div class="veil"></div>
									<?php } ?>

									<?php if($slider_6_tipo == 'fotografia' && $slider_6_imagen){ ?>
										<div class="photo cover" style="background-image:url('<?php echo $slider_6_imagen; ?>')"></div>
									<?php } ?>
								</div><!-- slide -->
							<?php endwhile; ?>
						</div><!-- slider-6-nav -->
					</section><!-- slider-6-container -->
				<?php endif; //slider_6 ?>


			<?php elseif ( get_row_layout() == 'bloque_2' ) : ?>
				<?php if ( have_rows( 'bloque_2' ) ) : ?>
					<?php
					while ( have_rows( 'bloque_2' ) ) : the_row();
					$bloque_2_imagen_de_fondo = get_sub_field( 'bloque_2_imagen_de_fondo' );
					$bloque_2_disposicion = get_sub_field( 'bloque_2_disposicion' );

					//Contenido principal
					$bloque_2_contenido_pre_titulo = get_sub_field( 'bloque_2_contenido_pre_titulo' );
					$bloque_2_contenido_titulo = get_sub_field( 'bloque_2_contenido_titulo' );
					$bloque_2_contenido_descripcion = get_sub_field( 'bloque_2_contenido_descripcion' );
					$bloque_2_contenido_link = get_sub_field( 'bloque_2_contenido_link' );
					if( $bloque_2_contenido_link ){
						$bloque_2_contenido_link_url = $bloque_2_contenido_link['url'];
						$bloque_2_contenido_link_title = $bloque_2_contenido_link['title'];
						$bloque_2_contenido_link_target = $bloque_2_contenido_link['target'] ? $bloque_2_contenido_link['target'] : '_self';
					}

					//Contenido mini
					$bloque_2_contenido_mini_imagen = get_sub_field( 'bloque_2_contenido_mini_imagen' );
					$bloque_2_contenido_mini_pre_titulo = get_sub_field( 'bloque_2_contenido_mini_pre_titulo' );
					$bloque_2_contenido_mini_titulo = get_sub_field( 'bloque_2_contenido_mini_titulo' );
					?>

						<section class="block-2 <?php echo $bloque_2_disposicion; ?>">
							<?php if($bloque_2_contenido_mini_imagen || $bloque_2_contenido_mini_pre_titulo || $bloque_2_contenido_mini_titulo){ ?>
								<div class="content-mini">
									<?php if($bloque_2_contenido_mini_imagen) { ?>
										<img src="<?php echo $bloque_2_contenido_mini_imagen; ?>">
									<?php } ?>

									<?php if($bloque_2_contenido_mini_pre_titulo){ ?>
										<p class="pre-title"><?php echo $bloque_2_contenido_mini_pre_titulo; ?></p>
									<?php } ?>

									<?php if($bloque_2_contenido_mini_titulo){ ?>
										<p class="title"><?php echo $bloque_2_contenido_mini_titulo; ?></p>
									<?php } ?>
								</div><!-- content-mini -->
							<?php } ?>

							<div class="content">
								<?php if($bloque_2_contenido_pre_titulo){ ?>
									<p class="pre-title"><?php echo $bloque_2_contenido_pre_titulo; ?></p>
								<?php } ?>

								<?php if($bloque_2_contenido_titulo){ ?>
									<p class="title"><?php echo $bloque_2_contenido_titulo; ?></p>
								<?php } ?>

								<?php if($bloque_2_contenido_descripcion){ ?>
									<div class="description">
										<?php echo $bloque_2_contenido_descripcion; ?>
									</div><!-- description -->
								<?php } ?>

								<?php if($bloque_2_contenido_link){ ?>
									<a href="<?php echo $bloque_2_contenido_link_url; ?>" class="button" target="<?php echo $bloque_2_contenido_link_target; ?>">
										<?php echo $bloque_2_contenido_link_title; ?>
									</a>
								<?php } ?>
							</div><!-- content -->

							<?php if($bloque_2_imagen_de_fondo){ ?>
								<div class="photo cover" style="background-image:url('<?php echo $bloque_2_imagen_de_fondo; ?>')"></div>
							<?php } ?>

							<div class="veil"></div>
						</section><!-- block-2 -->
					<?php endwhile; ?>
				<?php endif; //bloque_2 ?>

			<?php elseif ( get_row_layout() == 'bloque_3' ) : ?>
				<section class="block-3-container">
					<?php
					$bloque_3_columnas = get_sub_field( 'bloque_3_columnas' );
					$bloque_3_imagen = get_sub_field( 'bloque_3_imagen' );
					$bloque_3_pre_titulo = get_sub_field( 'bloque_3_pre_titulo' );
					$bloque_3_titulo = get_sub_field( 'bloque_3_titulo' );
					$bloque_3_descripcion = get_sub_field( 'bloque_3_descripcion' );
					$bloque_3_columnas = get_sub_field( 'bloque_3_columnas' );
					if($bloque_3_pre_titulo || $bloque_3_titulo || $bloque_3_descripcion){
					?>
						<div class="main-content">
							<?php if($bloque_3_pre_titulo){ ?>
								<p class="pre-title"><?php echo $bloque_3_pre_titulo; ?></p>
							<?php } ?>

							<?php if($bloque_3_titulo){ ?>
								<p class="title"><?php echo $bloque_3_titulo; ?></p>
							<?php } ?>

							<?php if($bloque_3_descripcion){ ?>
								<div class="description">
									<?php echo $bloque_3_descripcion; ?>
								</div>
							<?php } ?>
						</div>
					<?php } ?>

					<?php if ( have_rows( 'bloque_3_cifras' ) ) : ?>
						<div class="gap-l <?php echo $bloque_3_columnas; ?>">
							<?php
							while ( have_rows( 'bloque_3_cifras' ) ) : the_row();
							$bloque_3_cifras_icono = get_sub_field( 'bloque_3_cifras_icono' );
							$bloque_3_cifras_precifra = get_sub_field( 'bloque_3_cifras_precifra' );
							$bloque_3_cifras_cifra = get_sub_field( 'bloque_3_cifras_cifra' );
							$bloque_3_cifras_postcifra = get_sub_field( 'bloque_3_cifras_postcifra' );
							$bloque_3_cifras_titulo = get_sub_field( 'bloque_3_cifras_titulo' );
							$bloque_3_cifras_post_titulo = get_sub_field( 'bloque_3_cifras_post_titulo' );
							?>
							<div class="block-3">
								<div class="content">
									<?php if($bloque_3_cifras_precifra || $bloque_3_cifras_cifra || $bloque_3_cifras_postcifra || $bloque_3_cifras_titulo || $bloque_3_cifras_post_titulo){ ?>
										<div class="counter-item">
											<?php if($bloque_3_cifras_precifra || $bloque_3_cifras_cifra || $bloque_3_cifras_postcifra){ ?>
												<p class="count-container">
													<?php if($bloque_3_cifras_precifra){ ?>
														<span><?php echo $bloque_3_cifras_precifra; ?></span>
													<?php } ?>

													<?php if($bloque_3_cifras_cifra){ ?>
														<span class="count"><?php echo $bloque_3_cifras_cifra; ?></span>
													<?php } ?>

													<?php if($bloque_3_cifras_postcifra){ ?>
														<span><?php echo $bloque_3_cifras_postcifra; ?></span>
													<?php } ?>
												</p>
											<?php } ?>

											<?php if($bloque_3_cifras_titulo){ ?>
												<p class="title"><?php echo $bloque_3_cifras_titulo; ?></p>
											<?php } ?>

											<?php if($bloque_3_cifras_post_titulo){ ?>
												<div class="post-title"><?php echo $bloque_3_cifras_post_titulo; ?></div>
											<?php } ?>
										</div><!-- counter-item -->
									<?php } ?>
								</div><!-- content -->
							</div><!-- block-3 -->
							<?php endwhile; ?>
						</div><!-- grid-column-x -->
					<?php endif; //bloque_3_cifras ?>

					<?php if($bloque_3_imagen){ ?>
						<div class="photo cover" style="background-image:url('<?php echo $bloque_3_imagen; ?>')"></div>
					<?php } ?>
					<div class="veil"></div>
				</section><!-- block-3-container -->

			<?php elseif ( get_row_layout() == 'bloque_4' ) :
				$bloque_4_imagen = get_sub_field( 'bloque_4_imagen' );
				$bloque_4_autor = get_sub_field( 'bloque_4_autor' );
				$bloque_4_cita = get_sub_field( 'bloque_4_cita' );
			?>
				<section class="block-4">
					<div class="content">
						<?php echo $bloque_4_cita; ?>
						<?php if($bloque_4_autor) { ?>
							<p class="author"><?php echo $bloque_4_autor; ?></p>
						<?php } ?>
					</div>

					<?php if($bloque_4_imagen) { ?>
						<div class="photo cover" style="background-image:url('<?php echo $bloque_4_imagen; ?>');"></div>
					<?php } ?>

					<div class="veil"></div>
				</section><!-- block-4 -->

			<?php elseif ( get_row_layout() == 'bloque_5' ) : ?>
					<?php if ( have_rows( 'bloque_5' ) ) : ?>
						<ul class="block-5">
						<?php
						while ( have_rows( 'bloque_5' ) ) : the_row();
						$bloque_5_pregunta = get_sub_field( 'bloque_5_pregunta' );
						$bloque_5_respuesta = get_sub_field( 'bloque_5_respuesta' );
						?>
							<li>
								<?php if($bloque_5_pregunta){ ?>
									<div class="trigger">
										<?php echo $bloque_5_pregunta; ?>
										<i class="icon-mas"></i>
									</div>
								<?php } ?>

								<?php if($bloque_5_pregunta){ ?>
									<div class="wysiwyg">
										<p><?php echo $bloque_5_pregunta; ?></p>
									</div>
								<?php } ?>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>


					<?php
					elseif ( get_row_layout() == 'bloque_6' ) :
						$count = 0;
						$locations = get_sub_field('bloque_6');
						if (is_array($locations)) {
							$count = count($locations);
						}
					?>
						<section class="block-6 col-100 left clearfix">
							<div class="wrap-xl">
								<?php
								if ( have_rows( 'bloque_6' ) ) :
								?>
									<div class="grid-column-<?php echo $count; ?> gap-l">
										<?php
										$i = 1;
										while (have_rows('bloque_6')) : the_row();
										$bloque_6_estilo = get_sub_field( 'bloque_6_estilo' );
										$bloque_6_icono = get_sub_field( 'bloque_6_icono' );
										$bloque_6_cifra = get_sub_field( 'bloque_6_cifra' );
										$bloque_6_titulo = get_sub_field('bloque_6_titulo');
										$bloque_6_descripcion = get_sub_field('bloque_6_descripcion');
										?>
											<div class="block">
												<?php if($bloque_6_estilo == 'icono' && $bloque_6_icono){ ?>
													<img src="<?php echo $bloque_6_icono; ?>">
												<?php }elseif($bloque_6_estilo == 'numeracion-automatica'){ ?>
													<p class="number"><?php echo $i; ?></p>
												<?php }elseif($bloque_6_estilo == 'cifra'){ ?>
													<p class="number count">
														<?php echo $bloque_6_cifra; ?>
													</p>
												<?php } ?>

												<?php if($bloque_6_titulo){ ?>
													<p class="title"><?php echo $bloque_6_titulo; ?></p>
												<?php } ?>

												<?php if($bloque_6_descripcion){ ?>
													<div class="description"><?php echo $bloque_6_descripcion; ?></div>
												<?php } ?>
											</div>
										<?php
										$i++;
										endwhile;
										?>
									</div>
								<?php endif; ?>
							</div>
						</section>
					<?php endif; ?>

		<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer() ?>
