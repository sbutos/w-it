<?php
/* Template Name: Átomos */
get_header();
?>

	<section id="pesos">
		<div class="tabs">
			<p class="primary-title">Tipografía</p>
			<p class="secondary-title">Pesos</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<p class="block thin">Thin</p>
				<p class="block extra-light">Extra light</p>
				<p class="block light">Light</p>
				<p class="block regular">Regular</p>
				<p class="block medium">Medium</p>
				<p class="block bold">Bold</p>
				<p class="block extra-bold">Extra bold</p>
				<p class="block black">Black</p>
				<p class="block italic">Italic</p>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;p class="thin"&gt;Thin&lt;/p&gt;
&lt;p class="extra-light"&gt;Extra light&lt;/p&gt;
&lt;p class="light"&gt;Light&lt;/p&gt;
&lt;p class="regular"&gt;Regular&lt;/p&gt;
&lt;p class="medium"&gt;Medium&lt;/p&gt;
&lt;p class="bold"&gt;Bold&lt;/p&gt;
&lt;p class="extra-bold"&gt;Extra bold&lt;/p&gt;
&lt;p class="black"&gt;Black&lt;/p&gt;
&lt;p class="italic"&gt;Italic&lt;/p&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_fonts.scss */

/*------------------------------------------------------*/
/*------------------------ PESOS -----------------------*/
/*------------------------------------------------------*/
.thin{
  font-weight: 100;
}

.extra-light{
  font-weight: 200;
}

.light{
  font-weight: 300;
}

.regular{
  font-weight: 400;
}

.medium{
  font-weight: 500;
}

.bold{
  font-weight: 700;
}

.extra-bold{
  font-weight: 800;
}

.black{
  font-weight: 900;
}

.italic{
  font-style: italic;
}
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
p{
  @extend .thin;
}
/*Y así sucesivamente con todas las clases creadas*/

					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #pesos -->


	<section id="alineacion">
		<div class="tabs">
			<p class="secondary-title">Alineación</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<p class="block uppercase">Texto en mayúsculas</p>
				<p class="block text-center">Texto centrado</p>
				<p class="block text-left">Texto alineado a la izquierda</p>
				<p class="block text-right">Texto alineado a la derecha</p>
				<p class="block text-justify">Ejemplo de un texto justificado: Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto. Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo. Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. - ¿Qué me ha ocurrido? No estaba soñando. Su habitación, una habitación normal, aunque muy pequeña, tenía el aspecto habitual. Sobre la mesa había desparramado un muestrario de paños</p>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;p class="uppercase"&gt;Texto en mayúsculas&lt;/p&gt;
&lt;p class="text-center"&gt;Texto centrado&lt;/p&gt;
&lt;p class="text-left"&gt;Texto alineado a la izquierda&lt;/p&gt;
&lt;p class="text-right"&gt;Texto alineado a la derecha&lt;/p&gt;
&lt;p class="text-justify"&gt;Texto justificado&lt;/p&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_fonts.scss */

/*------------------------------------------------------*/
/*------------------ ALINEACIÓN TEXTO ------------------*/
/*------------------------------------------------------*/
.uppercase{
  text-transform: uppercase;
}

.text-center{
  text-align: center;
}

.text-left{
  text-align: left;
}

.text-right{
  text-align: right
}

.text-justify{
  text-align: justify;
}
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
p{
  @extend .uppercase;
}
/*Y así sucesivamente con todas las clases creadas*/
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #alineacion -->


	<section id="tamanos">
		<div class="tabs">
			<p class="secondary-title">Tamaños</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>Existen 90 tamaños tipográficos de clase <strong>.font-$i</strong>, donde el primero es <strong>.font-10</strong> y el último es <strong>.font-100</strong>.</p>
				</div>

				<p class="block font-10">Tipografía</p>
				<p class="block font-11">Tipografía</p>
				<p class="block font-12">Tipografía</p>
				<p class="block font-13">Tipografía</p>
				<p class="block font-14">Tipografía</p>
				<p class="block font-15">Tipografía</p>
				<p class="block font-16">Tipografía</p>
				<p class="block font-17">Tipografía</p>
				<p class="block font-18">Tipografía</p>
				<p class="block font-19">Tipografía</p>
				<p class="block font-20">Tipografía</p>

				<p class="block font-21">Tipografía</p>
				<p class="block font-22">Tipografía</p>
				<p class="block font-23">Tipografía</p>
				<p class="block font-24">Tipografía</p>
				<p class="block font-25">Tipografía</p>
				<p class="block font-26">Tipografía</p>
				<p class="block font-27">Tipografía</p>
				<p class="block font-28">Tipografía</p>
				<p class="block font-29">Tipografía</p>
				<p class="block font-30">Tipografía</p>

				<p class="block font-31">Tipografía</p>
				<p class="block font-32">Tipografía</p>
				<p class="block font-33">Tipografía</p>
				<p class="block font-34">Tipografía</p>
				<p class="block font-35">Tipografía</p>
				<p class="block font-36">Tipografía</p>
				<p class="block font-37">Tipografía</p>
				<p class="block font-38">Tipografía</p>
				<p class="block font-39">Tipografía</p>
				<p class="block font-40">Tipografía</p>

				<p class="block font-41">Tipografía</p>
				<p class="block font-42">Tipografía</p>
				<p class="block font-43">Tipografía</p>
				<p class="block font-44">Tipografía</p>
				<p class="block font-45">Tipografía</p>
				<p class="block font-46">Tipografía</p>
				<p class="block font-47">Tipografía</p>
				<p class="block font-48">Tipografía</p>
				<p class="block font-49">Tipografía</p>
				<p class="block font-50">Tipografía</p>

				<p class="block font-51">Tipografía</p>
				<p class="block font-52">Tipografía</p>
				<p class="block font-53">Tipografía</p>
				<p class="block font-54">Tipografía</p>
				<p class="block font-55">Tipografía</p>
				<p class="block font-56">Tipografía</p>
				<p class="block font-57">Tipografía</p>
				<p class="block font-58">Tipografía</p>
				<p class="block font-59">Tipografía</p>
				<p class="block font-60">Tipografía</p>

				<p class="block font-61">Tipografía</p>
				<p class="block font-62">Tipografía</p>
				<p class="block font-63">Tipografía</p>
				<p class="block font-64">Tipografía</p>
				<p class="block font-65">Tipografía</p>
				<p class="block font-66">Tipografía</p>
				<p class="block font-67">Tipografía</p>
				<p class="block font-68">Tipografía</p>
				<p class="block font-69">Tipografía</p>
				<p class="block font-70">Tipografía</p>

				<p class="block font-71">Tipografía</p>
				<p class="block font-72">Tipografía</p>
				<p class="block font-73">Tipografía</p>
				<p class="block font-74">Tipografía</p>
				<p class="block font-75">Tipografía</p>
				<p class="block font-76">Tipografía</p>
				<p class="block font-77">Tipografía</p>
				<p class="block font-78">Tipografía</p>
				<p class="block font-79">Tipografía</p>
				<p class="block font-80">Tipografía</p>

				<p class="block font-81">Tipografía</p>
				<p class="block font-82">Tipografía</p>
				<p class="block font-83">Tipografía</p>
				<p class="block font-84">Tipografía</p>
				<p class="block font-85">Tipografía</p>
				<p class="block font-86">Tipografía</p>
				<p class="block font-87">Tipografía</p>
				<p class="block font-88">Tipografía</p>
				<p class="block font-89">Tipografía</p>
				<p class="block font-90">Tipografía</p>

				<p class="block font-91">Tipografía</p>
				<p class="block font-92">Tipografía</p>
				<p class="block font-93">Tipografía</p>
				<p class="block font-94">Tipografía</p>
				<p class="block font-95">Tipografía</p>
				<p class="block font-96">Tipografía</p>
				<p class="block font-97">Tipografía</p>
				<p class="block font-98">Tipografía</p>
				<p class="block font-99">Tipografía</p>
				<p class="block font-100">Tipografía</p>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;p class="font-10"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-11"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-12"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-13"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-14"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-15"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-16"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-17"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-18"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-19"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-20"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-21"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-22"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-23"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-24"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-25"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-26"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-27"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-28"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-29"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-30"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-31"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-32"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-33"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-34"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-35"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-36"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-37"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-38"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-39"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-40"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-41"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-42"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-43"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-44"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-45"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-46"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-47"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-48"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-49"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-50"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-51"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-52"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-53"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-54"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-55"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-56"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-57"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-58"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-59"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-60"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-61"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-62"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-63"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-64"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-65"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-66"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-67"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-68"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-69"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-70"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-71"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-72"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-73"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-74"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-75"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-76"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-77"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-78"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-79"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-80"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-81"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-82"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-83"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-84"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-85"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-86"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-87"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-88"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-89"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-90"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-91"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-92"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-93"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-94"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-95"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-96"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-97"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-98"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-99"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-100"&gt;Tipografía&lt;/p&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_fonts.scss */

/*------------------------------------------------------*/
/*--------------------- TAMAÑOS  -----------------------*/
/*------------------------------------------------------*/
.font-10{
  font-size: 10px;
  line-height: 140%;
}

.font-32{
  font-size: 32px;
  line-height: 120%;
}

/* Y así sucesivamente hasta el 100... */

.font-100{
  font-size: 100px;
  line-height: 120%;
}
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/*
Se realiza un for que recorre del 10 al 100. A cada una le aplica un font-size del for que recorre, obteniendo así 90 tamaños tipográficos.

Existe la excepción de cuando la fuente es mayor a 21, el line-height cambia de 140% a 120%
*/

$font: 1px;
@for $i from 10 through 100{
    .font-#{$i}{
        font-size: $font*$i;
        @if $i < 21{
            line-height: 140%;
        }@else{
            line-height: 120%;
        }
    }
}

/* Para usarlo, por ejemplo: */
p{
  @extend .font-32;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #tamanos -->



	<section id="titles">
		<div class="tabs">
			<p class="primary-title">Editor WYSIWYG</p>
			<p class="secondary-title">Títulos</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<section class="wysiwyg">
					<h1>Título número h1</h1>
					<h2>Título número h2</h2>
					<h3>Título número h3</h3>
					<h4>Título número h4</h4>
					<h5>Título número h5</h5>
					<h6>Título número h6</h6>
				</section>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="wysiwyg"&gt;
  &lt;h1&gt;Título número h1&lt;/h1&gt;
  &lt;h2&gt;Título número h2&lt;/h2&gt;
  &lt;h3&gt;Título número h3&lt;/h3&gt;
  &lt;h4&gt;Título número h4&lt;/h4&gt;
  &lt;h5&gt;Título número h5&lt;/h5&gt;
  &lt;h6&gt;Título número h6&lt;/h6&gt;
&lt;/section&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_wysiwyg.scss */

.wysiwyg{
  /*------------------------------------------------------*/
  /*----------------------- TÍTULOS ----------------------*/
  /*------------------------------------------------------*/
  @for $i from 1 through 6{
    h#{$i}{
      @extend .bold;
      @if $i == 1{
        font-size: $font * 42;
      }
      @elseif $i < 3{
        font-size: $font * 42 / ($i/1.7);
      }@else{
        font-size: ($font * 42) / ($i/2.25);
      }
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #titles -->




	<section id="paragraph">
		<div class="tabs">
			<p class="secondary-title">Párrafo</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>Para incorporar estilos a aquellos campos que existen en el edior WYSIWYG de Wordpress, se debe encapsular todo en un elemento de clase <strong>.wysiwyg</strong></p>
				</div>

				<section class="wysiwyg">
					<p>Simulación de un <a href="http://google.cl">link</a>, un texto en <strong>negrita</strong> y también en <em>itálica</em>. Finalmente, y por qué no, el <del>texto tachado</del>.</p>

					<p><strong>Párrafo alineado a la izquierda: </strong>Estaba echado de espaldas sobre este es un link un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</p>

					<p style="text-align: right;"><strong>Párrafo alineado a la derecha: </strong>Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. – ¿Qué me ha ocurrido? No estaba soñando. Su habitación, una habitación normal, aunque muy pequeña, tenía el aspecto habitual.</p>

					<p style="text-align: center;"><strong>Párrafo alineado al centro: </strong>Sobre la mesa había desparramado un muestrario de paños – Samsa era viajante de comercio-, y de la pared colgaba una estampa recientemente recortada de una revista ilustrada y puesta en un marco dorado.</p>

					<p>
						<img class=" alignleft" src="https://i.etsystatic.com/13030919/r/il/792e79/1146518143/il_1588xN.1146518143_f6ui.jpg">
					</p>

					<div id="idTextPanel" class="jqDnR">
						<p><strong>Párrafo de texto con fotografía a la izquierda: </strong>Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.</p>
						<p>Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</p>
						<p>Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. – ¿Qué me ha ocurrido? No estaba soñando. Su habitación, una habitación normal, aunque muy pequeña, tenía el aspecto habitual.</p>
						<p>Sobre la mesa había desparramado un muestrario de paños – Samsa era viajante de comercio-, y de la pared colgaba una estampa recientemente recortada de una revista ilustrada y puesta en un marco dorado.</p>
						<p>La estampa mostraba a una mujer tocada con un gorro de pieles, envuelta en una estola también de pieles, y que, muy erguida, esgrimía un amplio manguito, asimismo de piel, que ocultaba todo su antebrazo. Gregorio miró hacia la ventana; estaba nublado, y sobre el cinc del alféizar repiqueteaban las gotas de lluvia, lo que le hizo sentir una gran melancolía.</p>
						<p>«Bueno -pensó-; ¿y si siguiese durmiendo un rato y me olvidase de todas estas locuras? » Pero no era posible, pues Gregorio tenía la costumbre de dormir sobre el lado derecho, y su actual estado no le permitía adoptar tal postura. Por más que se esforzara volvía a quedar de espaldas. Intentó en vano esta operación numerosas veces; cerró los ojos para no tener que ver aquella confusa agitación de patas, que no cesó hasta que notó en el costado un dolor leve y punzante, un dolor jamás sentido hasta entonces. – ¡Qué cansada es la profesión que he elegido! -se dijo-. Siempre de viaje. Las preocupaciones son mucho mayores cuando se trabaja</p>
					</div>

					<p>
					<img class="alignright" src="https://www.ancient-origins.net/sites/default/files/Merlin-the-wizard.jpg">
					<strong>Párrafo de texto con fotografía a la derecha: </strong>Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.</p>
					<p>Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</p>
					<p>Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. – ¿Qué me ha ocurrido? No estaba soñando. Su habitación, una habitación normal, aunque muy pequeña, tenía el aspecto habitual.</p>
					<p>Sobre la mesa había desparramado un muestrario de paños – Samsa era viajante de comercio-, y de la pared colgaba una estampa recientemente recortada de una revista ilustrada y puesta en un marco dorado.</p>
					<p>La estampa mostraba a una mujer tocada con un gorro de pieles, envuelta en una estola también de pieles, y que, muy erguida, esgrimía un amplio manguito, asimismo de piel, que ocultaba todo su antebrazo. Gregorio miró hacia la ventana; estaba nublado, y sobre el cinc del alféizar repiqueteaban las gotas de lluvia, lo que le hizo sentir una gran melancolía.</p>
					<p>«Bueno -pensó-; ¿y si siguiese durmiendo un rato y me olvidase de todas estas locuras? » Pero no era posible, pues Gregorio tenía la costumbre de dormir sobre el lado derecho, y su actual estado no le permitía adoptar tal postura. Por más que se esforzara volvía a quedar de espaldas. Intentó en vano esta operación numerosas veces; cerró los ojos para no tener que ver aquella confusa agitación de patas, que no cesó hasta que notó en el costado un dolor leve y punzante, un dolor jamás sentido hasta entonces. – ¡Qué cansada es la profesión que he elegido! -se dijo-. Siempre de viaje. Las preocupaciones son mucho mayores cuando se trabaja</p>

					<blockquote><p>La estampa mostraba a una mujer tocada con un gorro de pieles, envuelta en una estola también de pieles, y que, muy erguida, esgrimía un amplio manguito, asimismo de piel, que ocultaba todo su antebrazo. Gregorio miró hacia la ventana; estaba nublado, y sobre el cinc del alféizar repiqueteaban las gotas de lluvia, lo que le hizo sentir una gran melancolía.</p></blockquote>
				</section>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="wysiwyg"&gt;
  &lt;p&gt;Párrafo alineado a la izquierda.&lt;/p&gt;
  &lt;p style="text-align: right;"&gt;Párrafo alineado a la derecha.&lt;/p&gt;
  &lt;p style="text-align: center;"&gt;Párrafo alineado al centro.&lt;/p&gt;
  &lt;p&gt;
    &lt;img class=" alignleft" src="https://i.etsystatic.com/13030919/r/il/792e79/1146518143/il_1588xN.1146518143_f6ui.jpg"&gt;
  &lt;/p&gt;
  &lt;p&gt;Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.&lt;/p&gt;
  &lt;p&gt;Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.&lt;/p&gt;
  &lt;p&gt;Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. – ¿Qué me ha ocurrido? No estaba soñando. Su habitación, una habitación normal, aunque muy pequeña, tenía el aspecto habitual.&lt;/p&gt;
  &lt;p&gt;Sobre la mesa había desparramado un muestrario de paños – Samsa era viajante de comercio-, y de la pared colgaba una estampa recientemente recortada de una revista ilustrada y puesta en un marco dorado.&lt;/p&gt;
  &lt;p&gt;La estampa mostraba a una mujer tocada con un gorro de pieles, envuelta en una estola también de pieles, y que, muy erguida, esgrimía un amplio manguito, asimismo de piel, que ocultaba todo su antebrazo. Gregorio miró hacia la ventana; estaba nublado, y sobre el cinc del alféizar repiqueteaban las gotas de lluvia, lo que le hizo sentir una gran melancolía.&lt;/p&gt;
  &lt;p&gt;
    &lt;img class="alignright" src="https://www.ancient-origins.net/sites/default/files/Merlin-the-wizard.jpg"&gt;
  &lt;p&gt;
  &lt;p&gt;Sobre la mesa había desparramado un muestrario de paños – Samsa era viajante de comercio-, y de la pared colgaba una estampa recientemente recortada de una revista ilustrada y puesta en un marco dorado.&lt;/p&gt;
  &lt;p&gt;La estampa mostraba a una mujer tocada con un gorro de pieles, envuelta en una estola también de pieles, y que, muy erguida, esgrimía un amplio manguito, asimismo de piel, que ocultaba todo su antebrazo. Gregorio miró hacia la ventana; estaba nublado, y sobre el cinc del alféizar repiqueteaban las gotas de lluvia, lo que le hizo sentir una gran melancolía.&lt;/p&gt;
  &lt;p&gt;«Bueno -pensó-; ¿y si siguiese durmiendo un rato y me olvidase de todas estas locuras? » Pero no era posible, pues Gregorio tenía la costumbre de dormir sobre el lado derecho, y su actual estado no le permitía adoptar tal postura. Por más que se esforzara volvía a quedar de espaldas. Intentó en vano esta operación numerosas veces; cerró los ojos para no tener que ver aquella confusa agitación de patas, que no cesó hasta que notó en el costado un dolor leve y punzante, un dolor jamás sentido hasta entonces. – ¡Qué cansada es la profesión que he elegido! -se dijo-. Siempre de viaje. Las preocupaciones son mucho mayores cuando se trabaja&lt;/p&gt;
  &lt;blockquote&gt;
    &lt;p&gt;La estampa mostraba a una mujer tocada con un gorro de pieles, envuelta en una estola también de pieles, y que, muy erguida, esgrimía un amplio manguito, asimismo de piel, que ocultaba todo su antebrazo. Gregorio miró hacia la ventana; estaba nublado, y sobre el cinc del alféizar repiqueteaban las gotas de lluvia, lo que le hizo sentir una gran melancolía.&lt;/p&gt;
  &lt;/blockquote&gt;
&lt;/section&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_wysiwyg.scss */

/*------------------------------------------------------*/
/*------------------------ MEDIA -----------------------*/
/*------------------------------------------------------*/
img{
  height: auto;
  &.alignleft,
  &.alignright{
    width: 35%;
  }
  &.alignleft{
    float: left;
    margin: 0 18px 18px 0;
  }
  &.alignright{
    float: right;
    margin: 0 0 18px 18px;
  }
  &.aligncenter{
    margin: 0 auto;
    display: block;
  }
  &.alignnone{
    @extend .wrap-xl;
  }
}

/*------------------------------------------------------*/
/*------------------------- CITA -----------------------*/
/*------------------------------------------------------*/
blockquote{
  padding-left: 2.5%;
  margin-top: 20px;
  margin-bottom: 20px;
  @extend .italic;
  @extend .wrap-xl;
  @extend .relative;
  &:before{
    position: absolute;
    height: calc(100% - 46px);
    top: 26px;
    left: 0;
    width: 4px;
    background-color: $negro;
  }
  p,ol,ul{
    @extend .font-32;
  }
}
/*------------------------------------------------------*/
/*---------------------- CONTENIDO ---------------------*/
/*------------------------------------------------------*/
p,ol,ul{
  @extend .font-16;
  margin: 18px 0;
}
a{
  padding: 0 5px;
  border-bottom: 1px dotted;
  @extend .bold;
  &:hover{
    border-bottom: 1px solid;
    box-shadow: inset 0px -50px 0 0 $negro;
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #paragraph -->





	<section id="media">
		<div class="tabs">
			<p class="secondary-title">Media</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>Para incorporar estilos a aquellos campos que existen en el edior WYSIWYG de Wordpress, se debe encapsular todo en un elemento de clase <strong>.wysiwyg</strong></p>
					<br>
					<p>En el caso de las galerías fotográficas, se ocupa la librería <a href="https://sachinchoolur.github.io/lightGallery/" target="_blank">lightgallery.js</a>.</p>
				</div>

				<section class="wysiwyg">
					<p><img class="aligncenter size-medium" src="https://i.pinimg.com/originals/73/9f/74/739f74e0950ef5f9244406891e0280c0.jpg"></p>
					<p><img class="alignnone size-medium" src="https://www.ancient-origins.net/sites/default/files/field/image/Merlin-the-Magician.jpg"></p>
					<p><iframe title="Merlín - El Mago (Documental)" src="https://www.youtube.com/embed/wZLURWX48zk?feature=oembed" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="500" height="281" frameborder="0"></iframe></p>

					<div id="gallery-1" class="gallery galleryid-3 gallery-columns-3 gallery-size-thumbnail"><dl class="gallery-item">
						<dt class="gallery-icon portrait">
							<a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1.jpg" class="gallery-link"><img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" width="400" height="400"></a>
						</dt></dl><dl class="gallery-item">
						<dt class="gallery-icon landscape">
							<a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-6.jpg" class="gallery-link"><img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-6-400x360.jpg" class="attachment-thumbnail size-thumbnail" alt="" width="400" height="360"></a>
						</dt></dl><dl class="gallery-item">
						<dt class="gallery-icon portrait">
							<a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg" class="gallery-link"><img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" width="400" height="400"></a>
						</dt></dl><br style="clear: both"><dl class="gallery-item">
						<dt class="gallery-icon portrait">
							<a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-4.jpg" class="gallery-link"><img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-4-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" width="400" height="400"></a>
						</dt></dl><dl class="gallery-item">
						<dt class="gallery-icon landscape">
							<a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-3.jpg" class="gallery-link"><img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-3-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" width="400" height="400"></a>
						</dt></dl><dl class="gallery-item">
						<dt class="gallery-icon portrait">
							<a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-2.jpg" class="gallery-link"><img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-2.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-30" width="220" height="272"></a>
						</dt>
						</dl>
					</div>
				</section>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="wysiwyg"&gt;
  &lt;p&gt;
    &lt;img class="aligncenter size-medium" src="https://i.pinimg.com/originals/73/9f/74/739f74e0950ef5f9244406891e0280c0.jpg"&gt;
  &lt;/p&gt;
  &lt;p&gt;
    &lt;img class="alignnone size-medium" src="https://www.ancient-origins.net/sites/default/files/field/image/Merlin-the-Magician.jpg"&gt;
  &lt;/p&gt;
  &lt;p&gt;
    &lt;iframe title="Merlín - El Mago (Documental)" src="https://www.youtube.com/embed/wZLURWX48zk?feature=oembed" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="500" height="281" frameborder="0"&gt;&lt;/iframe&gt;
  &lt;/p&gt;

  &lt;div id="gallery-1" class="gallery galleryid-3 gallery-columns-3 gallery-size-thumbnail"&gt;
    &lt;dl class="gallery-item"&gt;
      &lt;dt class="gallery-icon portrait"&gt;
        &lt;a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1.jpg" class="gallery-link"&gt;
          &lt;img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" width="400" height="400"&gt;
        &lt;/a&gt;
      &lt;/dt&gt;
    &lt;/dl&gt;
    &lt;dl class="gallery-item"&gt;
      &lt;dt class="gallery-icon landscape"&gt;
        &lt;a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-6.jpg" class="gallery-link"&gt;
          &lt;img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-6-400x360.jpg" class="attachment-thumbnail size-thumbnail" alt="" width="400" height="360"&gt;
        &lt;/a&gt;
      &lt;/dt&gt;
    &lt;/dl&gt;
    &lt;dl class="gallery-item"&gt;
      &lt;dt class="gallery-icon portrait"&gt;
        &lt;a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg" class="gallery-link"&gt;
          &lt;img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" width="400" height="400"&gt;
        &lt;/a&gt;
      &lt;/dt&gt;
    &lt;/dl&gt;
    &lt;dl class="gallery-item"&gt;
      &lt;dt class="gallery-icon portrait"&gt;
        &lt;a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-4.jpg" class="gallery-link"&gt;
          &lt;img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-4-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" width="400" height="400"&gt;
        &lt;/a&gt;
      &lt;/dt&gt;
    &lt;/dl&gt;
    &lt;dl class="gallery-item"&gt;
      &lt;dt class="gallery-icon landscape"&gt;
        &lt;a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-3.jpg" class="gallery-link"&gt;
          &lt;img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-3-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" width="400" height="400"&gt;
        &lt;/a&gt;
      &lt;/dt&gt;
    &lt;/dl&gt;
    &lt;dl class="gallery-item"&gt;
      &lt;dt class="gallery-icon portrait"&gt;
        &lt;a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-2.jpg" class="gallery-link"&gt;
          &lt;img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-2.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-30" width="220" height="272"&gt;
        &lt;/a&gt;
      &lt;/dt&gt;
    &lt;/dl&gt;
  &lt;/div&gt;
&lt;/section&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_wysiwyg.scss */

img{
  height: auto;
  &.alignleft,
  &.alignright{
    width: 35%;
  }
  &.alignleft{
    float: left;
    margin: 0 18px 18px 0;
  }
  &.alignright{
    float: right;
    margin: 0 0 18px 18px;
  }
  &.aligncenter{
    margin: 0 auto;
    display: block;
  }
  &.alignnone{
    @extend .wrap-xl;
  }
}
iframe{
  height: 60vh;
  @extend .wrap-xl;
}
.gallery{
  grid-gap: $font * 10;
  .gallery-item{
    width: 100% !important;
    margin: 0 !important;
    height: 20vh;
    overflow: hidden;
    @extend .relative;
    .gallery-icon{
      @extend .left;
      img{
        top: 0;
        left: 0;
        height: auto;
        width: 100%;
        right: 0;
        bottom: 0;
        margin: auto;
        @extend .transition-slower;
      }
      a{
        padding: 0;
        width: 100%;
        height: 100%;
        border: none;
        @extend .absolute;
        &:hover{
          img{
            @include scale(1.15);
          }
          &:hover{
            box-shadow: none;
          }
        }
      }
    }
    .gallery-caption{
      display: none;
    }
  }
  br{
    display: none;
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #media -->




	<section id="lists">
		<div class="tabs">
			<p class="secondary-title">Listas</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>Para incorporar estilos a aquellos campos que existen en el edior WYSIWYG de Wordpress, se debe encapsular todo en un elemento de clase <strong>.wysiwyg</strong></p>
				</div>

				<section class="wysiwyg">
					<ul>
						<li>Elemento de la lista #1</li>
						<li>Elemento de la lista #2</li>
						<li>Elemento de la lista #3</li>
						<li>Elemento de la lista #4</li>
					</ul>

					<ol>
						<li>Elemento de la lista #1</li>
						<li>Elemento de la lista #2</li>
						<li>Elemento de la lista #3</li>
						<li>Elemento de la lista #4</li>
					</ol>
				</section>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="wysiwyg"&gt;
  &lt;ul&gt;
    &lt;li&gt;Elemento de la lista #1&lt;/li&gt;
    &lt;li&gt;Elemento de la lista #2&lt;/li&gt;
    &lt;li&gt;Elemento de la lista #3&lt;/li&gt;
    &lt;li&gt;Elemento de la lista #4&lt;/li&gt;
  &lt;/ul&gt;

  &lt;ol&gt;
    &lt;li&gt;Elemento de la lista #1&lt;/li&gt;
    &lt;li&gt;Elemento de la lista #2&lt;/li&gt;
    &lt;li&gt;Elemento de la lista #3&lt;/li&gt;
    &lt;li&gt;Elemento de la lista #4&lt;/li&gt;
  &lt;/ol&gt;
&lt;/section&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_wysiwyg.scss */

/*------------------------------------------------------*/
/*------------------------ LISTAS ----------------------*/
/*------------------------------------------------------*/
ul,ol{
  padding-left: 7%;
  li{
    margin: 4px 0;

  }
}
ul{
  li{
    @extend .relative;
    @include icomoon("icon-chevron-derecha");
    &:before{
      font-size: $font * 9;
      color: $blanco;
      background-color: $negro;
      width: 10px;
      height: 10px;
      left: -18px;
      top: 6.5px;
      @extend .border-radius-xl;
      @extend .absolute;
    }
  }
}
ol{
  list-style: decimal;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #lists -->


	<section id="transitions">
		<div class="tabs">
			<p class="primary-title">Animación</p>
			<p class="secondary-title">Transiciones</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<p class="block no-transition">Sin transición (simular al hover)</p>
				<p class="block transition">Transición rápida, 0.25s (simular al hover)</p>
				<p class="block transition-slow">Transición media, 0.5s (simular al hover)</p>
				<p class="block transition-slower">Transición lenta, 0.75s (simular al hover)</p>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;p class="no-transition"&gt;Sin transición (simular al hover)&lt;/p&gt;
&lt;p class="transition"&gt;Transición rápida, 0.25s (simular al hover)&lt;/p&gt;
&lt;p class="transition-slow"&gt;Transición media, 0.5s (simular al hover)&lt;/p&gt;
&lt;p class="transition-slower"&gt;Transición lenta, 0.75s (simular al hover)&lt;/p&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación del @mixin: components/merlin/scss/_mixins.scss */

@mixin transition($ms) {
  -webkit-transition: all $ms ease-in-out;
  -moz-transition: all $ms ease-in-out;
  -ms-transition: all $ms ease-in-out;
  -o-transition: all $ms ease-in-out;
  transition: all $ms ease-in-out;
}

/* Ubicación del @mixin: components/merlin/scss/_animations.scss */

/*------------------------------------------------------*/
/*-------------------- TRANSICIONES --------------------*/
/*------------------------------------------------------*/
$ms: 0.25s;

.no-transition{
  transition: 0;
}

a,
input,
textarea,
select,
input:focus,
textarea:focus,
select:focus,
.transition{
  transition: $ms;
}

.transition-slow{
  transition: $ms*2;
}

.transition-slower{
  transition: $ms*3;
}

					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #transitions -->


	<section id="zoom-in">
		<div class="tabs">
			<p class="secondary-title">Zoom in</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="image-container relative overflow-hidden">
					<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
				</div><!-- image-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="image-container relative overflow-hidden"&gt;
  &lt;div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"&gt;&lt;/div&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_animations.scss */

/*------------------------------------------------------*/
/*----------------------- ZOOM IN ----------------------*/
/*------------------------------------------------------*/
.zoom{
  animation: zoom $ms * 125 infinite;
  -ms-animation: zoom $ms * 125 infinite;
  -moz-animation: zoom $ms * 125 infinite;
  -webkit-animation: zoom $ms * 125 infinite;
}

  @keyframes zoom{
    0%{
      transform: scale(1);
    }
    50%{
      transform: scale(1.15);
    }
    100%{
      transform: scale(1);
    }
  }

  @-webkit-keyframes zoom{
    0%{
      -webkit-transform: scale(1);
    }
    50%{
      -webkit-transform: scale(1.15);
    }
    100%{
      -webkit-transform: scale(1);
    }
  }

  @-moz-keyframes zoom{
    0%{
      -moz-transform: scale(1);
    }
    50%{
      -moz-transform: scale(1.15);
    }
    100%{
      -moz-transform: scale(1);
    }
  }
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #zoom-in -->



	<section id="zoom-out">
		<div class="tabs">
			<p class="secondary-title">Zoom out</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="image-container relative overflow-hidden">
					<div class="photo cover zoom-out" style="background-image:url('https://source.unsplash.com/random')"></div>
				</div><!-- image-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="image-container relative overflow-hidden"&gt;
  &lt;div class="photo cover zoom-out" style="background-image:url('https://source.unsplash.com/random')"&gt;&lt;/div&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_animations.scss */

/*------------------------------------------------------*/
/*---------------------- ZOOM OUT ----------------------*/
/*------------------------------------------------------*/
.zoom-out{
  animation: zoom-out $ms * 125 infinite;
  -ms-animation: zoom-out $ms * 125 infinite;
  -moz-animation: zoom-out $ms * 125 infinite;
  -webkit-animation: zoom-out $ms * 125 infinite;
}

@keyframes zoom-out{
  0%{
    transform: scale(1.15);
  }
  50%{
    transform: scale(1);
  }
  100%{
    transform: scale(1.15);
  }
}

@-webkit-keyframes zoom-out{
  0%{
    -webkit-transform: scale(1.15);
  }
  50%{
    -webkit-transform: scale(1);
  }
  100%{
    -webkit-transform: scale(1.15);
  }
}

@-moz-keyframes zoom-out{
  0%{
    -moz-transform: scale(1.15);
  }
  50%{
    -moz-transform: scale(1);
  }
  100%{
    -moz-transform: scale(1.15);
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #zoom-out -->


	<section id="zoom-left-top">
		<div class="tabs">
			<p class="secondary-title">Zoom left top</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<p class="message-block text-center">Esta clase es dependiente de <strong>.zoom</strong></p>
				<div class="image-container relative overflow-hidden">
					<div class="photo cover zoom zoom-left-top" style="background-image:url('https://source.unsplash.com/random')"></div>
				</div><!-- image-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
/*Importante: Esta clase es dependiente de la clase .zoom*/
&lt;div class="image-container relative overflow-hidden"&gt;
  &lt;div class="photo cover zoom zoom-left-top" style="background-image:url('https://source.unsplash.com/random')"&gt;&lt;/div&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_animations.scss */

.zoom-left-top{
  transform-origin: 0% 0%;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #zoom-left-top -->



	<section id="zoom-left-bottom">
		<div class="tabs">
			<p class="secondary-title">Zoom left bottom</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<p class="message-block text-center">Esta clase es dependiente de <strong>.zoom</strong></p>
				<div class="image-container relative overflow-hidden">
					<div class="photo cover zoom zoom-left-bottom" style="background-image:url('https://source.unsplash.com/random')"></div>
				</div><!-- image-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
/*Importante: Esta clase es dependiente de la clase .zoom*/
&lt;div class="image-container relative overflow-hidden"&gt;
  &lt;div class="photo cover zoom zoom-left-bottom" style="background-image:url('https://source.unsplash.com/random')"&gt;&lt;/div&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_animations.scss */

.zoom-left-bottom{
  transform-origin: 0% 100%;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #zoom-left-bottom -->


	<section id="zoom-right-top">
		<div class="tabs">
			<p class="secondary-title">Zoom right top</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<p class="message-block text-center">Esta clase es dependiente de <strong>.zoom</strong></p>
				<div class="image-container relative overflow-hidden">
					<div class="photo cover zoom zoom-right-top" style="background-image:url('https://source.unsplash.com/random')"></div>
				</div><!-- image-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
/*Importante: Esta clase es dependiente de la clase .zoom*/
&lt;div class="image-container relative overflow-hidden"&gt;
  &lt;div class="photo cover zoom zoom-right-top" style="background-image:url('https://source.unsplash.com/random')"&gt;&lt;/div&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_animations.scss */

.zoom-right-top{
  transform-origin: 100% 0%;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #zoom-right-top -->


	<section id="zoom-right-bottom">
		<div class="tabs">
			<p class="secondary-title">Zoom right bottom</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<p class="message-block text-center">Esta clase es dependiente de <strong>.zoom</strong></p>
				<div class="image-container relative overflow-hidden">
					<div class="photo cover zoom zoom-right-bottom" style="background-image:url('https://source.unsplash.com/random')"></div>
				</div><!-- image-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
/*Importante: Esta clase es dependiente de la clase .zoom*/
&lt;div class="image-container relative overflow-hidden"&gt;
  &lt;div class="photo cover zoom zoom-right-bottom" style="background-image:url('https://source.unsplash.com/random')"&gt;&lt;/div&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_animations.scss */

.zoom-right-bottom{
  transform-origin: 100% 100%;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #zoom-right-bottom -->




	<section id="border-radius">
		<div class="tabs">
			<p class="secondary-title">Border Radius</p>
			<ul class="tabs-triggers">
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación del @mixin: components/merlin/scss/_mixins.scss */
@mixin border-radius($top-left:null, $top-right:null, $bottom-right:null, $bottom-left:null){
    -webkit-border-radius: $top-left $top-right $bottom-right $bottom-left;
    -moz-border-radius: $top-left $top-right $bottom-right $bottom-left;
    -ms-border-radius: $top-left $top-right $bottom-right $bottom-left;
    border-radius: $top-left $top-right $bottom-right $bottom-left;  
}

/*------------------------------------------------------*/
/*--------------- ¿Cómo utilizar el mixin? -------------*/
/*------------------------------------------------------*/
.elemento{
    @include border-radius(10px);
}
.elemento-2{
    @include border-radius(10px 20px 30px 40px);
}


					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #border-radius -->



	<section id="parallax">
		<div class="tabs">
			<p class="secondary-title">Parallax</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<p class="message-block text-center">Se trabaja con la librería externa <a href="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/simpleParallax.min.js" target="_blank">simpleParallax.js</a>. Debes crear el elemento img de clase <strong>.parallax</strong>.</p>
				<div class="image-container relative overflow-hidden">
					<img class="parallax" src="https://source.unsplash.com/random">
				</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;img class="parallax" src="https://source.unsplash.com/random"&gt;
/*Importante: El plugin genera automáticamente un div que contiene al img de clase .simpleParallax. Los detalles de este div como el del img, se detallan en la pestaña CSS  */
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación del CSS: components/merlin/scss/_general.scss */

.simpleParallax{
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: 1;
}

.simpleParallax > * {
  -o-object-fit: cover;
  object-fit: cover;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  margin: auto;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #parallax -->




	<section id="grayscale">
		<div class="tabs">
			<p class="secondary-title">Filtro blanco y negro</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<p class="message-block text-center">Para aplicar el filtro blanco y negro, añadir la clase <strong>.grayscale</strong>. En caso de revertir el efecto, por ejemplo, al hacer un :hover, añadir la clase <strong>.no-grayscale</strong></p>
				<div class="image-container grayscale cover" style="background-image:url('https://source.unsplash.com/random');"></div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="grayscale" style="https://source.unsplash.com/random"&gt;&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación del CSS: components/merlin/scss/_general.scss */

/*------------------------------------------------------*/
/*---------------------- GRÁFICA -----------------------*/
/*------------------------------------------------------*/
.grayscale{
  -webkit-filter: grayscale(100%);
  filter: grayscale(100%);
}

					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #grayscale -->






	<section id="gradients">
		<div class="tabs">
			<p class="secondary-title">Gradientes</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#css">CSS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<p class="message-block text-center">Cada vez que se requiera generar un gradiente, se debe utilizar el <strong>mixin linear-gradient</strong>, el cual define un <strong>color inicial</strong>, un <strong>color final</strong> y un <strong>ángulo</strong>.</p>
				<div class="grid-column-3 gap-m">
					<div class="block block-gradient block-gradient-1 text-center">
						<p>Gradiente 0º</p>
					</div>
					<div class="block block-gradient block-gradient-2 text-center">
						<p>Gradiente 180º</p>
					</div>
					<div class="block block-gradient block-gradient-3 text-center">
						<p>Gradiente 58º</p>
					</div>
				</div><!-- grid-column-x -->
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación del @mixin: components/merlin/scss/_mixins.scss */
@mixin linear-gradient($color-one,$color-two,$degrees) {
  background: -moz-linear-gradient($degrees, $color-one 0%, $color-two 100%);
  background: -webkit-gradient(linear, left top, right top, color-stop(0%, $color-one), color-stop(100%, $color-two));
  background: -webkit-linear-gradient($degrees, $color-one 0%, $color-two 100%);
  background: -o-linear-gradient($degrees, $color-one 0%, $color-two 100%);
  background: -ms-linear-gradient($degrees, $color-one 0%, $color-two 100%);
  background: linear-gradient($degrees, $color-one 0%, $color-two 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='$color-one', endColorstr='$color-two',GradientType=1 );
}

/*------------------------------------------------------*/
/*--------------- ¿Cómo utilizar el mixin? -------------*/
/*------------------------------------------------------*/
.elemento{
  @include linear-gradient($color-inicial, $color-final, 0deg);
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #gradients -->





	<section id="social">
		<div class="tabs">
			<p class="primary-title">Íconos</p>
			<p class="secondary-title">Social</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>La familia tipográfica utiliza la herramienta <a href="https://icomoon.io/" target="_blank">icomoon.io</a>. Se puede descargar esta familia descargando el archivo <a href="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/icons/selection.json" target="_blank">selection.json</a> para complementarlo o simplemente usar los necesarios en tu proyecto.</p>
					<br>
					<p>Si eres diseñador, puedes <a href="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/icons/SVG.zip" target="_blank">descargarte los SVG</a> para comenzar a trabajar.</p>
				</div>

				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-pinterest"></i>
						<span>.icon-pinterest</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-skype"></i>
						<span>.icon-skype</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-spotify"></i>
						<span>.icon-spotify</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-whatsapp"></i>
						<span>.icon-whatsapp</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-facebook"></i>
						<span>.icon-facebook</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-instagram"></i>
						<span>.icon-instagram</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-soundcloud"></i>
						<span>.icon-soundcloud</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-twitter"></i>
						<span>.icon-twitter</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-youtube-isotipo"></i>
						<span>.icon-youtube-isotipo</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-linkedin"></i>
						<span>.icon-linkedin</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-youtube-logotipo"></i>
						<span>.icon-youtube-logotipo</span>
					</li>
				</ul>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;i class="icon-pinterest"&gt;&lt;/i&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/*
Ubicacion de la iconografía:
1. components/merlin/icons/icomoon.eot
2. components/merlin/icons/icomoon.svg
3. components/merlin/icons/icomoon.ttf
4. components/merlin/icons/icomoon.woff

Además, si se quiere complementar la familia, se puede descargar el archivo components/merlin/icons/selection.json

Ubicacion del sass: components/merlin/scss/icons.css
*/

/* Además, existe un @mixin ubicado en components/merlin/scss/mixins.css */
@mixin icomoon($icon, $position: "before", $replace: false) {
  @if $replace {
    font-size: 0;
  }
  &:#{$position} {
    @extend .#{$icon};
    font-family: 'icomoon';
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    @if $replace {
      font-size: $font * 16;
    }
    @content;
  }
}


/*
Este @mixin sirve para llamar a los íconos dentro del sass. Por ejemplo:
li{
	@include icomoon("icon-flecha-derecha-mediana");
}

*/
					</code>
				</pre>
			</div><!-- tab-content -->
	</section><!-- #social -->




	<section id="arrows-down">
		<div class="tabs">
			<p class="secondary-title">Flechas hacia abajo</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-flecha-abajo-mediana"></i>
						<span>.icon-flecha-abajo-mediana</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-chevron-abajo"></i>
						<span>.icon-chevron-abajor</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-flecha-abajo-circulo"></i>
						<span>.icon-flecha-abajo-circulo</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-chevrons-abajo"></i>
						<span>.icon-chevrons-abajo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-descargar-nube"></i>
						<span>.icon-descargar-nube</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-descargar"></i>
						<span>.icon-descargar</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #arrows-down -->



	<section id="arrows-left">
		<div class="tabs">
			<p class="secondary-title">Flechas hacia la izquierda</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-flecha-izquierda-mediana"></i>
						<span>.icon-flecha-izquierda-mediana</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-chevron-izquierda"></i>
						<span>.icon-chevron-izquierda</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flecha-izquierda-circulo"></i>
						<span>.icon-flecha-izquierda-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-chevrons-izquierda"></i>
						<span>.icon-chevrons-izquierda</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #arrows-left -->



	<section id="arrows-right">
		<div class="tabs">
			<p class="secondary-title">Flechas hacia la derecha</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-flecha-derecha-mediana"></i>
						<span>.icon-flecha-derecha-mediana</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-chevron-derecha"></i>
						<span>.icon-chevron-derecha</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flecha-derecha-circulo"></i>
						<span>.icon-flecha-derecha-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-chevrons-derecha"></i>
						<span>.icon-chevrons-derecha</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-log-in"></i>
						<span>.icon-log-in</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-log-out"></i>
						<span>.icon-log-out</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #arrows-right -->



	<section id="arrows-up">
		<div class="tabs">
			<p class="secondary-title">Flechas hacia arriba</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-flecha-arriba-mediana"></i>
						<span>.icon-flecha-arriba-mediana</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-chevron-arriba"></i>
						<span>.icon-chevron-arriba</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flecha-arriba-circulo"></i>
						<span>.icon-flecha-arriba-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-chevrons-arriba"></i>
						<span>.icon-chevrons-arriba</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-subir"></i>
						<span>.icon-subir</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-compartir"></i>
						<span>.icon-compartir</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-subir-nube"></i>
						<span>.icon-subir-nube</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #arrows-up -->




	<section id="arrows-directional">
		<div class="tabs">
			<p class="secondary-title">Flechas direccionales</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-refrescar"></i>
						<span>.icon-refrescar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-repetir"></i>
						<span>.icon-repetir</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-rotar-1"></i>
						<span>.icon-rotar-1</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-rotar-2"></i>
						<span>.icon-rotar-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flecha-arriba-izquierda"></i>
						<span>.icon-flecha-arriba-izquierda</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flecha-arriba-derecha"></i>
						<span>.icon-flecha-arriba-derecha</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flecha-abajo-izquierda"></i>
						<span>.icon-flecha-abajo-izquierda</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flecha-abajo-derecha"></i>
						<span>.icon-flecha-abajo-derecha</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-abajo-izquierda"></i>
						<span>.icon-virar-abajo-izquierda</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-abajo-derecha"></i>
						<span>.icon-virar-abajo-derecha</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-arriba-izquierda"></i>
						<span>.icon-virar-arriba-izquierda</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-arriba-derecha"></i>
						<span>.icon-virar-arriba-derecha</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-abajo"></i>
						<span>.icon-virar-abajo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-abajo-2"></i>
						<span>.icon-virar-abajo-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-arriba"></i>
						<span>.icon-virar-arriba</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-arriba-2"></i>
						<span>.icon-virar-arriba-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-tendencia-arriba"></i>
						<span>.icon-tendencia-arriba</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-tendencia-abajo"></i>
						<span>.icon-tendencia-abajo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-codigo"></i>
						<span>.icon-codigo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-external-link"></i>
						<span>.icon-external-link</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-maximizar-2"></i>
						<span>.icon-maximizar-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-minimizar-2"></i>
						<span>.icon-minimizar-2</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #arrows-directional -->





	<section id="controls">
		<div class="tabs">
			<p class="secondary-title">Controles</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-monitor"></i>
						<span>.icon-monitor</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-bandera"></i>
						<span>.icon-bandera</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-llave"></i>
						<span>.icon-llave</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-trello"></i>
						<span>.icon-trello</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-tv"></i>
						<span>.icon-tv</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-candado"></i>
						<span>.icon-candado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-candado-off"></i>
						<span>.icon-candado-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-campana-off"></i>
						<span>.icon-campana-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-campana"></i>
						<span>.icon-campana</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mas-horizontal"></i>
						<span>.icon-mas-horizontal</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mas-vertical"></i>
						<span>.icon-mas-vertical</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flash-off"></i>
						<span>.icon-flash-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flash"></i>
						<span>.icon-flash</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-camara-off"></i>
						<span>.icon-camara-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-camara"></i>
						<span>.icon-camara</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-cast"></i>
						<span>.icon-cast</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-ojo-off"></i>
						<span>.icon-ojo-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-ojo"></i>
						<span>.icon-ojo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-filtro"></i>
						<span>.icon-filtro</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-microfono-off"></i>
						<span>.icon-microfono-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-microfono"></i>
						<span>.icon-microfono</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-minimizar"></i>
						<span>.icon-minimizar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-maximizar"></i>
						<span>.icon-maximizar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-move"></i>
						<span>.icon-move</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-play"></i>
						<span>.icon-play</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-pausa-circulo"></i>
						<span>.icon-pausa-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-pausa"></i>
						<span>.icon-pausa</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-rebobinar-2"></i>
						<span>.icon-rebobinar-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-adelantar-2"></i>
						<span>.icon-adelantar-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-rebobinar"></i>
						<span>.icon-rebobinar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-adelantar"></i>
						<span>.icon-adelantar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-telefono"></i>
						<span>.icon-telefono</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-llamada"></i>
						<span>.icon-llamada</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-llamar"></i>
						<span>.icon-llamar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-llamada-recibida"></i>
						<span>.icon-llamada-recibida</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-phone-missed"></i>
						<span>.icon-phone-missed</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-telefono-off"></i>
						<span>.icon-telefono-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-phone-outgoing"></i>
						<span>.icon-phone-outgoing</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-power"></i>
						<span>.icon-power</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-configuracion"></i>
						<span>.icon-configuracion</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-escudo-off"></i>
						<span>.icon-escudo-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-escudo"></i>
						<span>.icon-escudo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-aleatorio"></i>
						<span>.icon-aleatorio</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-controles"></i>
						<span>.icon-controles</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-stop-circulo"></i>
						<span>.icon-stop-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-pulgar-abajo"></i>
						<span>.icon-pulgar-abajo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-pulgar-arriba"></i>
						<span>.icon-pulgar-arriba</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-video-off"></i>
						<span>.icon-video-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-video"></i>
						<span>.icon-video</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-volume-1"></i>
						<span>.icon-volume-1</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-volume-2"></i>
						<span>.icon-volume-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-volumen-off"></i>
						<span>.icon-volumen-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-volumen"></i>
						<span>.icon-volumen</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-wifi-off"></i>
						<span>.icon-wifi-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-wifi"></i>
						<span>.icon-wifi</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-zoom-in"></i>
						<span>.icon-zoom-in</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-zoom-out"></i>
						<span>.icon-zoom-out</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #controls -->




	<section id="weather">
		<div class="tabs">
			<p class="secondary-title">Clima</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-paragua"></i>
						<span>.icon-paragua</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-nube-llovizna"></i>
						<span>.icon-nube-llovizna</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-nube-rayo"></i>
						<span>.icon-nube-rayo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-nube-off"></i>
						<span>.icon-nube-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-nube-lluvia"></i>
						<span>.icon-nube-lluvia</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-nube-nieve"></i>
						<span>.icon-nube-nieve</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-nube"></i>
						<span>.icon-nube</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-gota"></i>
						<span>.icon-gota</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-luna"></i>
						<span>.icon-luna</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-estrella"></i>
						<span>.icon-estrella</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-sol"></i>
						<span>.icon-sol</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-amanecer"></i>
						<span>.icon-amanecer</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-atardecer"></i>
						<span>.icon-atardecer</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-viento"></i>
						<span>.icon-viento</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #weather -->




	<section id="persons">
		<div class="tabs">
			<p class="secondary-title">Personas</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-usuario"></i>
						<span>.icon-usuario</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-usuario-check"></i>
						<span>.icon-usuario-check</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-usuario-menos"></i>
						<span>.icon-usuario-menos</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-usuario-mas"></i>
						<span>.icon-usuario-mas</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-usuario-eliminar"></i>
						<span>.icon-usuario-eliminar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-usuarios"></i>
						<span>.icon-usuarios</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #persons -->




	<section id="content">
		<div class="tabs">
			<p class="secondary-title">Contenido</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-carpeta-menos"></i>
						<span>.icon-carpeta-menos</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-carpeta-mas"></i>
						<span>.icon-carpeta-mas</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-carpeta"></i>
						<span>.icon-carpeta</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-basurero-lineas"></i>
						<span>.icon-basurero-lineas</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-basurero"></i>
						<span>.icon-basurero</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-clip"></i>
						<span>.icon-clip</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-papelero"></i>
						<span>.icon-papelero</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-libro-abierto"></i>
						<span>.icon-libro-abierto</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-libro"></i>
						<span>.icon-libro</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-marcador-de-libro"></i>
						<span>.icon-marcador-de-libro</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-caja"></i>
						<span>.icon-caja</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-portapapeles"></i>
						<span>.icon-portapapeles</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-editar"></i>
						<span>.icon-editar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-editar-linea"></i>
						<span>.icon-editar-linea</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-editar-cuadrado"></i>
						<span>.icon-editar-cuadrado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-archivo-menos"></i>
						<span>.icon-archivo-menos</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-archivo-mas"></i>
						<span>.icon-archivo-mas</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-archivo-texto"></i>
						<span>.icon-archivo-texto</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-archivo"></i>
						<span>.icon-archivo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-inbox"></i>
						<span>.icon-inbox</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-paquete"></i>
						<span>.icon-paquete</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-impresora"></i>
						<span>.icon-impresora</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-link-2"></i>
						<span>.icon-link-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-link"></i>
						<span>.icon-link</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-alinear-centro"></i>
						<span>.icon-alinear-centro</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-alinear-justificado"></i>
						<span>.icon-alinear-justificado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-alinear-izquierda"></i>
						<span>.icon-alinear-izquierda</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-alinear-derecha"></i>
						<span>.icon-alinear-derecha</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #content -->




	<section id="alerts">
		<div class="tabs">
			<p class="secondary-title">Alertas</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-menos-circulo"></i>
						<span>.icon-menos-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-menos-cuadrado"></i>
						<span>.icon-menos-cuadrado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-menos"></i>
						<span>.icon-menos</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mas"></i>
						<span>.icon-mas</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-prohibido"></i>
						<span>.icon-prohibido</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-equis-circulo"></i>
						<span>.icon-equis-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-equis-octagono"></i>
						<span>.icon-equis-octagono</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-equis-cuadrado"></i>
						<span>.icon-equis-cuadrado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-alerta-circulo"></i>
						<span>.icon-alerta-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-alerta-octagono"></i>
						<span>.icon-alerta-octagono</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-alerta-triangulo"></i>
						<span>.icon-alerta-triangulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-ayuda-circulo"></i>
						<span>.icon-ayuda-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-info"></i>
						<span>.icon-info</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-equis"></i>
						<span>.icon-equis</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-check"></i>
						<span>.icon-check</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-check-cuadrado"></i>
						<span>.icon-check-cuadrado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-check-circle"></i>
						<span>.icon-check-circle</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #alerts -->



	<section id="misc">
		<div class="tabs">
			<p class="secondary-title">Misceláneo</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-arroba"></i>
						<span>.icon-arroba</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-medalla"></i>
						<span>.icon-medalla</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-grafico-barras-2"></i>
						<span>.icon-grafico-barras-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-grafico-barras"></i>
						<span>.icon-grafico-barras</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-bateria-cargando"></i>
						<span>.icon-bateria-cargando</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-bateria"></i>
						<span>.icon-bateria</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-bluetooth"></i>
						<span>.icon-bluetooth</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-maleta"></i>
						<span>.icon-maleta</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-calendario"></i>
						<span>.icon-calendario</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-reloj"></i>
						<span>.icon-reloj</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-brujula"></i>
						<span>.icon-brujula</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-copiar"></i>
						<span>.icon-copiar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-tarjeta-de-credito"></i>
						<span>.icon-tarjeta-de-credito</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-base-de-datos"></i>
						<span>.icon-base-de-datos</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-peso"></i>
						<span>.icon-peso</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-pelicula"></i>
						<span>.icon-pelicula</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-regalo"></i>
						<span>.icon-regalo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mundo"></i>
						<span>.icon-mundo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-grilla"></i>
						<span>.icon-grilla</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-disco-duro"></i>
						<span>.icon-disco-duro</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-hashtag"></i>
						<span>.icon-hashtag</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-audifonos"></i>
						<span>.icon-audifonos</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-corazon"></i>
						<span>.icon-corazon</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-imagen"></i>
						<span>.icon-imagen</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-capas"></i>
						<span>.icon-capas</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-layout"></i>
						<span>.icon-layout</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-lista"></i>
						<span>.icon-lista</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mail"></i>
						<span>.icon-mail</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-pin"></i>
						<span>.icon-pin</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mapa"></i>
						<span>.icon-mapa</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-menu"></i>
						<span>.icon-menu</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mensaje-circular"></i>
						<span>.icon-mensaje-circular</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mensaje-cuadrado"></i>
						<span>.icon-mensaje-cuadrado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-puntero"></i>
						<span>.icon-puntero</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-musica"></i>
						<span>.icon-musica</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-navigation-arriba"></i>
						<span>.icon-navigation-arriba</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-navegacion-diagonal"></i>
						<span>.icon-navegacion-diagonal</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-porcentaje"></i>
						<span>.icon-porcentaje</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-grafico-torta"></i>
						<span>.icon-grafico-torta</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-play-circulo"></i>
						<span>.icon-play-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mas-circulo"></i>
						<span>.icon-mas-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mas-cuadrado"></i>
						<span>.icon-mas-cuadrado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-radio"></i>
						<span>.icon-radio</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-rss"></i>
						<span>.icon-rss</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-diskette"></i>
						<span>.icon-diskette</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-tijera"></i>
						<span>.icon-tijera</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-buscar"></i>
						<span>.icon-buscar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mail-2"></i>
						<span>.icon-mail-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-servidor"></i>
						<span>.icon-servidor</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-compartir-2"></i>
						<span>.icon-compartir-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-bolsa-shopping"></i>
						<span>.icon-bolsa-shopping</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-carrito"></i>
						<span>.icon-carrito</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-sidebar"></i>
						<span>.icon-sidebar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-etiqueta"></i>
						<span>.icon-etiqueta</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-grabadora"></i>
						<span>.icon-grabadora</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-reloj-muneca"></i>
						<span>.icon-reloj-muneca</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #misc -->
<?php get_footer(); ?>
