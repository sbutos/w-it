<?php
/* Template Name: Organismos */
get_header();
?>

	<section id="slider-1">
		<div class="tabs">
			<p class="primary-title">Sliders</p>
			<p class="secondary-title">Slider #1</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s text-left">
					<p>Se pueden añadir 4 tipos de sliders:</p>
					<ol>
						<li>Fotografía de fondo</li>
						<li>Subir video<strong>*</strong>: Al div de clase <strong>.slide</strong>, añadirle la clase <strong>video</strong>.</li>
						<li>Video de YouTube <strong>*</strong>: Al div de clase <strong>.slide</strong>, añadirle la clase <strong>youtube</strong>.</li>
						<li>Video de Vimeo <strong>*</strong>: Al div de clase <strong>.slide</strong>, añadirle la clase <strong>vimeo</strong>.</li>
					</ol>
					<p><strong>*</strong> Se requiere subir una fotografía igualmente para que sea visualizada en dispositivos móviles</p>
				</div>

				<section class="slider-1">
					<div class="slide">
						<div class="content col-50 relative z-index-2">
							<div class="slider-numbers relative clearfix">
								<div class="slider-numbers relative"></div>
								<div class="timer relative">
									<figure class="counter col-1 transform-time"></figure>
								</div>
							</div><!-- slider-numbers -->

							<p class="pre-title" data-aos="fade-right">Texto de pre-título</p>
							<p class="title" data-aos="fade-right" data-aos-delay="300">Título principal</p>
							<div class="description" data-aos="fade-right" data-aos-delay="500">
								<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
							</div>
							<a href="#" class="button" data-aos="fade-right" data-aos-delay="650" data-aos-offset="-250">Leer más</a>
						</div><!-- content -->
						<div class="veil"></div>
						<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
					</div><!-- slide -->

					<div class="slide video">
						<div class="content col-50 relative z-index-2">
							<div class="slider-numbers relative clearfix">
								<div class="slider-numbers relative"></div>
								<div class="timer relative">
									<figure class="counter col-1 transform-time"></figure>
								</div>
							</div><!-- slider-numbers -->

							<p class="pre-title" data-aos="fade-right">Texto de pre-título</p>
							<p class="title" data-aos="fade-right" data-aos-delay="300">Título principal Título principal</p>
							<p class="description" data-aos="fade-right" data-aos-delay="500">Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia.</p>
							<a href="#" class="button" data-aos="fade-right" data-aos-delay="650" data-aos-offset="-250">Leer más</a>
						</div><!-- content -->
						<div class="veil"></div>

						<video controls loop playsinline muted preload="none">
							<source src="https://want.cl/merlin/wp-content/uploads/2020/01/about-us.mp4" type="video/mp4" autostart="false">
						</video>
						<div class="photo cover zoom" style="background-image:url('https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg')"></div>
					</div><!-- slide -->

					<div class="slide youtube">
						<div class="content col-50 relative z-index-2">
							<div class="slider-numbers relative clearfix">
								<div class="slider-numbers relative"></div>
								<div class="timer relative">
									<figure class="counter col-1 transform-time"></figure>
								</div>
							</div><!-- slider-numbers -->

							<p class="pre-title" data-aos="fade-right">Texto de pre-título</p>
							<p class="title" data-aos="fade-right" data-aos-delay="300">Título principal Título principal</p>
							<p class="description" data-aos="fade-right" data-aos-delay="500">Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia.</p>
							<a href="#" class="button" data-aos="fade-right" data-aos-delay="650" data-aos-offset="-250">Leer más</a>
						</div><!-- content -->
						<div class="veil"></div>
						<iframe class="embed-player" src="https://www.youtube.com/embed/aDaOgu2CQtI?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=aDaOgu2CQtI&start=0&mute=1&autoplay=0" frameborder="0" allowfullscreen></iframe>
						<div class="photo cover zoom" style="background-image:url('https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg')"></div>
					</div><!-- slide -->

					<div class="slide vimeo">
						<div class="content col-50 relative z-index-2">
							<div class="slider-numbers relative clearfix">
								<div class="slider-numbers relative"></div>
								<div class="timer relative">
									<figure class="counter col-1 transform-time"></figure>
								</div>
							</div><!-- slider-numbers -->

							<p class="pre-title" data-aos="fade-right">Texto de pre-título</p>
							<p class="title" data-aos="fade-right" data-aos-delay="300">Título principal Título principal</p>
							<p class="description" data-aos="fade-right" data-aos-delay="500">Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia.</p>
							<a href="#" class="button" data-aos="fade-right" data-aos-delay="650" data-aos-offset="-250">Leer más</a>
						</div><!-- content -->
						<div class="veil"></div>

						<iframe class="embed-player" src="https://player.vimeo.com/video/265045525?api=1&byline=0&portrait=0&title=0&background=1&mute=1&loop=1&autoplay=0&id=265045525" width="980" height="520" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						<div class="photo cover zoom" style="background-image:url('https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg')"></div>
					</div><!-- slide -->
				</section><!-- slider-1 -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #slider-1 -->



	<section id="slider-2">
		<div class="tabs">
			<p class="secondary-title">Slider #2</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<section class="slider-2 col-100 left clearfix relative">
					<div class="slide vertical" style="background-image:url('https://source.unsplash.com/100x300')"></div>
					<div class="slide horizontal" style="background-image:url('https://source.unsplash.com/300x100')"></div>
					<div class="slide horizontal" style="background-image:url('https://source.unsplash.com/300x120')"></div>
					<div class="slide vertical" style="background-image:url('https://source.unsplash.com/120x300')"></div>
					<div class="slide vertical" style="background-image:url('https://source.unsplash.com/110x310')"></div>
					<div class="slide horizontal" style="background-image:url('https://source.unsplash.com/310x110')"></div>
					<div class="slide horizontal" style="background-image:url('https://source.unsplash.com/310x110')"></div>
					<div class="slide vertical" style="background-image:url('https://source.unsplash.com/110x310')"></div>
				</section><!-- slider-2 -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #slider-2 -->



	<section id="slider-3">
		<div class="tabs">
			<p class="secondary-title">Slider #3</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<section class="slider-3-container col-100 left clearfix relative">
					<div class="slider-3 col-100 left clearfix relative">
						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="main-title">Título del módulo</p>
								<p class="year">1990</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
							</div><!-- content -->
							<div class="veil"></div>
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="main-title">Título del módulo</p>
								<p class="year">1991</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
							</div><!-- content -->
							<div class="veil"></div>
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="main-title">Título del módulo</p>
								<p class="year">1992</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
							</div><!-- content -->
							<div class="veil"></div>
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="main-title">Título del módulo</p>
								<p class="year">1993</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
							</div><!-- content -->
							<div class="veil"></div>
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="main-title">Título del módulo</p>
								<p class="year">1994</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
							</div><!-- content -->
							<div class="veil"></div>
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->
					</div><!-- slider-3 -->

					<ul class="slider-nav-3">
						<li>1990</li>
						<li>1991</li>
						<li>1992</li>
						<li>1993</li>
						<li>1994</li>
					</ul>
				</section><!-- slider-3-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #slider-3 -->




	<section id="slider-4">
		<div class="tabs">
			<p class="secondary-title">Slider #4</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<section class="slider-4-container col-100 left clearfix relative">
					<ul class="slider-nav-4">
						<li>
							<div class="photography cover" style="background-image:url('https://source.unsplash.com/random')"></div>
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título</p>
							</div>
						</li>

						<li>
							<div class="photography cover" style="background-image:url('https://source.unsplash.com/random')"></div>
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título</p>
							</div>
						</li>

						<li>
							<div class="photography cover" style="background-image:url('https://source.unsplash.com/random')"></div>
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título</p>
							</div>
						</li>

						<li>
							<div class="photography cover" style="background-image:url('https://source.unsplash.com/random')"></div>
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título</p>
							</div>
						</li>

						<li>
							<div class="photography cover" style="background-image:url('https://source.unsplash.com/random')"></div>
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título</p>
							</div>
						</li>
					</ul>

					<div class="slider-4 col-100 left clearfix relative">
						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->
					</div><!-- slider-4 -->
				</section><!-- slider-4-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #slider-4 -->


	<section id="slider-5">
		<div class="tabs">
			<p class="secondary-title">Slider #5 (igual al #4, pero con tabs)</p>
			<div id="example" class="tab-content">
				<section class="slider-5-container col-100 left clearfix relative">
					<ul class="slider-nav-5 grid-column-2">
						<li data-tab="0">
							<a href="#uno">
								<div class="photography cover" style="background-image:url('https://source.unsplash.com/random')"></div>
								<div class="content">
									<p class="pre-title">Pre-título</p>
									<p class="title">Título</p>
								</div>
							</a>
						</li>

						<li data-tab="1">
							<a href="#dos">
								<div class="photography cover" style="background-image:url('https://source.unsplash.com/random')"></div>
								<div class="content">
									<p class="pre-title">Pre-título</p>
									<p class="title">Título</p>
								</div>
							</a>
						</li>
					</ul>

					<div id="uno" class="slider-5 col-100 left clearfix relative">
						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->
					</div><!-- #uno -->

					<div id="dos" class="slider-5 col-100 left clearfix relative">
						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="pre-title">Pre-título 2</p>
								<p class="title">Título principal 2</p>
								<div class="description">
									<p>2 Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="pre-title">3 Pre-título</p>
								<p class="title">3 Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->
					</div><!-- #dos -->
				</section><!-- slider-4-container -->
			</div><!-- #example -->
		</div>
	</section><!-- #slider-5 -->



	<section id="slider-6">
		<div class="tabs">
			<p class="secondary-title">Slider #6</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<section class="slider-6-container">
					<div class="slider-6">
						<div class="slide">
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->

						<div class="slide video">
							<video controls loop playsinline preload="none">
								<source src="https://want.cl/merlin/wp-content/uploads/2020/01/about-us.mp4" type="video/mp4" autostart="false">
							</video>
							<div class="photo cover zoom" style="background-image:url('https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg')"></div>
						</div><!-- slide -->

						<div class="slide">
							<div class="photo cover zoom" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->

						<div class="slide vimeo">
							<iframe class="embed-player" src="https://player.vimeo.com/video/265045525?api=1&byline=0&portrait=0&title=0&controls=1&mute=0&loop=1&autoplay=0&id=265045525" width="980" height="520" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div><!-- slide -->

						<div class="slide youtube">
							<iframe class="embed-player" src="https://www.youtube.com/embed/aDaOgu2CQtI?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&showinfo=0&loop=1&playlist=aDaOgu2CQtI&start=0&mute=0&autoplay=0&rel=0&modestbranding=1" frameborder="0" allowfullscreen></iframe>
						</div><!-- slide -->

						<div class="slide youtube">
							<iframe class="embed-player" src="https://www.youtube.com/embed/1_Uk5SrE5tE?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=1_Uk5SrE5tE&start=0&mute=1&autoplay=0" frameborder="0" allowfullscreen></iframe>
						</div><!-- slide -->

						<div class="slide vimeo">
							<iframe class="embed-player" src="https://player.vimeo.com/video/259411563?api=1&byline=0&portrait=0&title=0&controls=1&mute=0&loop=1&autoplay=0&id=259411563" width="980" height="520" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div><!-- slide -->
					</div><!-- slider-6 -->

					<div class="slider-6-nav">
						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="photo cover" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="photo cover" style="background-image:url('https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="photo cover" style="background-image:url('https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="photo cover" style="background-image:url('https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="photo cover" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="photo cover" style="background-image:url('https://source.unsplash.com/random')"></div>
						</div><!-- slide -->
					</div><!-- slider-6-nav -->
				</section><!-- slider-6-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
					</code>
				</pre>
			</div><!-- tab-content -->

		</div><!-- tabs -->
	</section><!-- #slider-6 -->





	<section id="slider-7">
		<div class="tabs">
			<p class="secondary-title">Slider #7</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<section class="slider-7-container">
					<div class="slider-7 desktop">
						<div class="grid-column-3 gap-s">
							<div class="block">Bloque 1</div>
							<div class="block">Bloque 2</div>
							<div class="block">Bloque 3</div>
							<div class="block">Bloque 4</div>
							<div class="block">Bloque 5</div>
							<div class="block">Bloque 6</div>
						</div><!-- grid-column-3 -->

						<div class="grid-column-3 gap-s">
							<div class="block">Bloque 7</div>
							<div class="block">Bloque 8</div>
							<div class="block">Bloque 9</div>
							<div class="block">Bloque 10</div>
						</div><!-- grid-column-3 -->

						<div class="grid-column-3 gap-s">
							<div class="block">Bloque 7</div>
							<div class="block">Bloque 8</div>
							<div class="block">Bloque 9</div>
							<div class="block">Bloque 10</div>
						</div><!-- grid-column-3 -->

						<div class="grid-column-3 gap-s">
							<div class="block">Bloque 7</div>
							<div class="block">Bloque 8</div>
							<div class="block">Bloque 9</div>
							<div class="block">Bloque 10</div>
						</div><!-- grid-column-3 -->
					</div><!-- slider-7.desktop -->

					<div class="slider-numbers-container relative clearfix">
						<div class="slider-numbers relative"></div>
						<figure class="line"></figure>
					</div><!-- slider-numbers -->

					<div class="slider-7 mobile">
						<div class="block">Bloque 1</div>
						<div class="block">Bloque 2</div>
						<div class="block">Bloque 3</div>
						<div class="block">Bloque 4</div>
						<div class="block">Bloque 5</div>
						<div class="block">Bloque 6</div>
						<div class="block">Bloque 7</div>
						<div class="block">Bloque 8</div>
						<div class="block">Bloque 9</div>
						<div class="block">Bloque 10</div>
					</div><!-- slider-7.mobile -->
				</section><!-- slider-7-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #slider-6 -->


	<section id="block-1">
		<div class="tabs">
			<p class="primary-title">Bloques</p>
			<p class="secondary-title">Bloque #1</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="grid-column-3 gap-l">
					<a href="#" class="block-1">
						<div class="categories">
							<span>Categoría #1</span>
							<span>Categoría #2</span>
						</div>
						<div class="content">
							<p class="date">88 de Noviembre 2020</p>
							<p class="title">Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas</p>
							<p class="description">Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
						</div><!-- content -->
						<div class="button">Leer más</div>
						<div class="photo cover" style="background-image:url('https://source.unsplash.com/random')"></div>
						<div class="veil"></div>
					</a><!-- block-1 -->

					<a href="#" class="block-1">
						<div class="categories">
							<span>Categoría #1</span>
						</div>
						<div class="content">
							<p class="date">88 de Noviembre 2020</p>
							<p class="title">Reina en mi espíritu una alegría admirable</p>
							<p class="description">Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
						</div><!-- content -->
						<div class="button">Leer más</div>
						<div class="photo cover" style="background-image:url('https://source.unsplash.com/random')"></div>
						<div class="veil"></div>
					</a><!-- block-1 -->

					<a href="#" class="block-1">
						<div class="categories">
							<span>Categoría #2</span>
						</div>
						<div class="content">
							<p class="date">88 de Noviembre 2020</p>
							<p class="title">Ahora no sabría dibujar, ni siquiera hacer una línea con el lápiz; y, sin embargo, jamás he sido mejor pintor</p>
							<p class="description">Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
						</div><!-- content -->
						<div class="button">Leer más</div>
						<div class="photo cover" style="background-image:url('https://source.unsplash.com/random')"></div>
						<div class="veil"></div>
					</a><!-- block-1 -->
				</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #block-1 -->




	<section id="block-2">
		<div class="tabs">
			<p class="secondary-title">Bloque #2</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s text-center">
					<p>Se puede posicionar la columna principal de texto hacia la izquierda o derecha, agregando la clase <strong>.align-left</strong> ó <strong>.align-right</strong> al div de clase <strong>.content</strong>.</p>
				</div>

				<section class="block-2 align-left">
					<div class="content-mini">
						<img src="https://source.unsplash.com/100/50">
						<p class="pre-title">Jamás he sido mejor pintor</p>
						<p class="title">Reina en mi espíritu una alegría admirable, muy parecida.</p>
					</div><!-- content-mini -->

					<div class="content">
						<p class="pre-title">Jamás he sido mejor pintor Cuando el valle se vela</p>
						<p class="title">Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia.</p>
						<div class="description">
							<p>Ahora no sabría dibujar, ni siquiera hacer una línea con el lápiz; y, sin embargo, jamás he sido mejor pintor Cuando el valle se vela en torno mío con un encaje de vapores; cuando el sol de mediodía centellea sobre la impenetrable sombra de mi bosque sin conseguir otra cosa que filtrar entre las hojas algunos rayos que penetran hasta el fondo del santuario.</p>
						</div><!-- description -->
						<div class="button">Leer más</div>
					</div><!-- content -->

					<div class="photo cover" style="background-image:url('https://source.unsplash.com/random')"></div>
					<div class="veil"></div>
				</section><!-- block-2 -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #block-2 -->



	<section id="block-3">
		<div class="tabs">
			<p class="secondary-title">Bloque #3</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s text-center">
					<p>Para utilizar este módulo, es necesario contar con los scripts <a href="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/count-up.js" target="_blank">count-up.js</a>, <a href="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/in-view.js" target="_blank">in-view.js</a>, además del <a href="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/block-3.js" target="_blank">script propio</a> del módulo.</p>
				</div>

				<section class="block-3-container">
					<div class="main-content">
						<p class="pre-title">Pre-título</p>
						<p class="title">Título</p>
						<div class="description">
							<p>Ahora no sabría dibujar, ni siquiera hacer una línea con el lápiz; y, sin embargo, jamás he sido mejor pintor Cuando el valle se vela en torno mío con un encaje de vapores; cuando el sol de mediodía centellea sobre la impenetrable sombra de mi bosque sin conseguir otra cosa que filtrar entre las hojas algunos rayos que penetran hasta el fondo del santuario.</p>
						</div>
					</div>

					<div class="grid-column-3 gap-l">
						<div class="block-3">
							<div class="content">
								<div class="counter-item">
									<i class="icon-ojo"></i>
									<span class="count">87%</span>
								</div>
								<p class="title">Título</p>
								<p class="post-title">Post-título</p>
							</div><!-- content -->
						</div><!-- block-3 -->

						<div class="block-3">
							<div class="content">
								<div class="counter-item">
									<i class="icon-monitor"></i>
									<span class="count">81.382.113</span>
								</div>
								<p class="title">Título</p>
								<p class="post-title">Post-título</p>
							</div><!-- content -->
						</div><!-- block-3 -->

						<div class="block-3">
							<div class="content">
								<div class="counter-item">
									<i class="icon-cast"></i>
									<span class="count">M112</span>
								</div>
								<p class="title">Título</p>
								<p class="post-title">Post-título</p>
							</div><!-- content -->
						</div><!-- block-3 -->
					</div><!-- grid-column-x -->

					<div class="photo cover" style="background-image:url('https://source.unsplash.com/random')"></div>
					<div class="veil"></div>
				</section><!-- block-3-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="counter-item"&gt;
  &lt;i class="icon-ojo"&gt;&lt;/i&gt;
  &lt;span class="count"&gt;87%&lt;/span&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #block-3 -->


	<section id="block-4">
		<div class="tabs">
			<p class="secondary-title">Bloque #4</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<section class="block-4">
					<div class="content">
						<p>Ahora no sabría dibujar, ni siquiera hacer una línea con el lápiz; y, sin embargo, jamás he sido mejor pintor Cuando el valle se vela en torno mío con un encaje de vapores; cuando el sol de mediodía centellea sobre la impenetrable sombra de mi bosque sin conseguir otra cosa que filtrar entre las hojas algunos rayos que penetran hasta el fondo del santuario.</p>
						<p class="author">Merlín, el Mago</p>
					</div>
					<img class="parallax" src="https://source.unsplash.com/random">
					<div class="veil"></div>
				</section><!-- block-4 -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #block-4 -->


	<section id="block-5">
		<div class="tabs">
			<p class="secondary-title">Bloque #5</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="block-5">
					<li>
						<div class="trigger">
							Pregunta
							<i class="icon-mas"></i>
						</div>

						<div class="wysiwyg">
							<p>Respuesta</p>
						</div>
					</li>

					<li>
						<div class="trigger">
							Pregunta 2
							<i class="icon-mas"></i>
						</div>

						<div class="wysiwyg">
							<p>Respuesta 2</p>
						</div>
					</li>

					<li>
						<div class="trigger">
							Pregunta 3
							<i class="icon-mas"></i>
						</div>

						<div class="wysiwyg">
							<p>Respuesta 3</p>
						</div>
					</li>
				</ul>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #block-5 -->


	<section id="block-6">
		<div class="tabs">
			<p class="secondary-title">Bloque #6</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s text-center">
					<p>Este módulo hereda archivos javascript del <a href="#block-3" class="scroll-move">Bloque #3</a> para hacer el conteo de cifras en caso que se requiera. Éstos son: <a href="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/count-up.js" target="_blank">count-up.js</a>, <a href="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/in-view.js" target="_blank">in-view.js</a>, además del <a href="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/block-3.js" target="_blank">script propio</a> del módulo anteriormente mencionado.</p>
				</div>

				<div class="block-6 grid-column-4 gap-s">
					<div class="block">
						<img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1.jpg">
						<p class="title">Título uno</p>
						<div class="description">
							<p>Y, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro.</p>
						</div>
					</div>

					<div class="block">
						<p class="number">2</p>
						<p class="title">Título 2</p>
						<div class="description">
							<p>Y, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro.</p>
						</div>
					</div>

					<div class="block">
						<p class="number count">98</p>
						<p class="title">Título tres</p>
						<div class="description">
							<p>Y, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro.</p>
						</div>
					</div>

					<div class="block">
						<p class="title">Título cuatro</p>
						<div class="description">
							<p>Y, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro.</p>
						</div>
					</div>
				</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #block-6 -->
<?php get_footer(); ?>
