<?php
/*-------------------------------------------------------------*/
/*---------------------- Opciones ACF -------------------------*/
/*-------------------------------------------------------------*/
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}
/*-------------------------------------------------------------*/
/*--- No mostrar la version de Wordpress dentro del <head> ----*/
/*-------------------------------------------------------------*/
function eliminar_version_wordpress() {
return '';
}
add_filter('the_generator', 'eliminar_version_wordpress');


/*-------------------------------------------------------------*/
/*-------------- Eliminar barra de administración -------------*/
/*-------------------------------------------------------------*/
function quitar_barra_administracion()	{
	return false;
}
add_filter( 'show_admin_bar' , 'quitar_barra_administracion');

/*-------------------------------------------------------------*/
/*---------------------- Eliminar Tags ------------------------*/
/*-------------------------------------------------------------*/
add_action('init', 'remove_tags');
function remove_tags(){
    register_taxonomy('post_tag', array());
}

/*-------------------------------------------------------------*/
/*--------- Permito imagen destacada en los Posts -------------*/
/*-------------------------------------------------------------*/
if ( function_exists( 'add_theme_support' ) )
add_theme_support( 'post-thumbnails' );

/*-------------------------------------------------------------*/
/*------------------ Menús personalizados ---------------------*/
/*-------------------------------------------------------------*/
register_nav_menus( array(
    'Menu principal' => 'Menu principal',
    'Menu secundario' => 'Menu secundario',
	'Menu footer' => 'Menu footer',
));

/*-------------------------------------------------------------*/
/*--- Le añado la clase "active" al elemento actual del menu ---*/
/*-------------------------------------------------------------*/
add_filter('nav_menu_css_class', function ($classes, $item, $args, $depth) {
    // all the different "active" classes added by WordPress
    $active = [
        'current-menu-item',
        'current-menu-parent',
        'current-menu-ancestor',
        'current_page_item'
    ];
    // if anything matches, add the "active" class
    if ( array_intersect( $active, $classes ) ) {
        $classes[] = 'active';
    }
    return $classes;
}, 10, 4);

/*----------------------------------------------------------------------------*/
/*---------------------------- Eliminar Gutenberg ----------------------------*/
/*----------------------------------------------------------------------------*/
add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_post_type', '__return_false', 10);

/*-------------------------------------------------------------*/
/*--------- Compatibilidad del tema con Woocommerce -----------*/
/*-------------------------------------------------------------*/
function my_theme_setup() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'my_theme_setup' );

/*-------------------------------------------------------------*/
/*------------- Auto <p> Tag en Contact Form 7 ----------------*/
/*-------------------------------------------------------------*/
add_filter('wpcf7_autop_or_not', '__return_false');

/*-------------------------------------------------------------*/
/*------------------- Custom Post Types -----------------------*/
/*-------------------------------------------------------------*/
// Register Custom Post Type Servicios
function custom_post_type_servicios() {

	$labels = array(
		'name'                  => _x( 'Servicios', 'Post Type General Name', 'wit' ),
		'singular_name'         => _x( 'Servicio', 'Post Type Singular Name', 'wit' ),
		'menu_name'             => __( 'Servicios', 'wit' ),
		'name_admin_bar'        => __( 'Servicios', 'wit' ),
		'archives'              => __( 'Archivo', 'wit' ),
		'attributes'            => __( 'Atributos', 'wit' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'wit' ),
		'all_items'             => __( 'Todos', 'wit' ),
		'add_new_item'          => __( 'Añadir nuevo', 'wit' ),
		'add_new'               => __( 'Añadir nuevo', 'wit' ),
		'new_item'              => __( 'Nuevo', 'wit' ),
		'edit_item'             => __( 'Editar', 'wit' ),
		'update_item'           => __( 'Actualizar', 'wit' ),
		'view_item'             => __( 'Ver', 'wit' ),
		'view_items'            => __( 'Ver', 'wit' ),
		'search_items'          => __( 'Buscar', 'wit' ),
		'not_found'             => __( 'No encontrado', 'wit' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'wit' ),
		'featured_image'        => __( 'Imagen Destacada', 'wit' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'wit' ),
		'remove_featured_image' => __( 'Remove featured image', 'wit' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'wit' ),
		'insert_into_item'      => __( 'Insertar', 'wit' ),
		'uploaded_to_this_item' => __( 'Cargar', 'wit' ),
		'items_list'            => __( 'Lista de Items', 'wit' ),
		'items_list_navigation' => __( 'Items list navigation', 'wit' ),
		'filter_items_list'     => __( 'Filtrar lista', 'wit' ),
	);
	$args = array(
		'label'                 => __( 'Servicio', 'wit' ),
		'description'           => __( 'Servicios W-it', 'wit' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-slides',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'servicios', $args );

}
add_action( 'init', 'custom_post_type_servicios', 0 );

// Register Custom Post Type Soluciones
function custom_post_type_soluciones() {

	$labels = array(
		'name'                  => _x( 'Soluciones', 'Post Type General Name', 'wit' ),
		'singular_name'         => _x( 'Solución', 'Post Type Singular Name', 'wit' ),
		'menu_name'             => __( 'Soluciones', 'wit' ),
		'name_admin_bar'        => __( 'Soluciones', 'wit' ),
		'archives'              => __( 'Archivo', 'wit' ),
		'attributes'            => __( 'Atributos', 'wit' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'wit' ),
		'all_items'             => __( 'Todos', 'wit' ),
		'add_new_item'          => __( 'Añadir nuevo', 'wit' ),
		'add_new'               => __( 'Añadir nuevo', 'wit' ),
		'new_item'              => __( 'Nuevo', 'wit' ),
		'edit_item'             => __( 'Editar', 'wit' ),
		'update_item'           => __( 'Actualizar', 'wit' ),
		'view_item'             => __( 'Ver', 'wit' ),
		'view_items'            => __( 'Ver', 'wit' ),
		'search_items'          => __( 'Buscar', 'wit' ),
		'not_found'             => __( 'No encontrado', 'wit' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'wit' ),
		'featured_image'        => __( 'Imagen Destacada', 'wit' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'wit' ),
		'remove_featured_image' => __( 'Remove featured image', 'wit' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'wit' ),
		'insert_into_item'      => __( 'Insertar', 'wit' ),
		'uploaded_to_this_item' => __( 'Cargar', 'wit' ),
		'items_list'            => __( 'Lista de Items', 'wit' ),
		'items_list_navigation' => __( 'Items list navigation', 'wit' ),
		'filter_items_list'     => __( 'Filtrar lista', 'wit' ),
	);
	$args = array(
		'label'                 => __( 'Soluciones', 'wit' ),
		'description'           => __( 'Soluciones W-it', 'wit' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'			=> array( 'tipo_solucion' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-sos',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'soluciones', $args );

}
add_action( 'init', 'custom_post_type_soluciones', 0 );

// Register Custom Taxonomy Tipo Solucion
function custom_taxonomy_tipo_solucion() {

	$labels = array(
		'name'                       => _x( 'Soluciones', 'Taxonomy General Name', 'wit' ),
		'singular_name'              => _x( 'Solución', 'Taxonomy Singular Name', 'wit' ),
		'menu_name'                  => __( 'Soluciones', 'wit' ),
		'all_items'                  => __( 'Todos', 'wit' ),
		'parent_item'                => __( 'Item Relacionado', 'wit' ),
		'parent_item_colon'          => __( 'Item relacionado:', 'wit' ),
		'new_item_name'              => __( 'Nombre Nuevo', 'wit' ),
		'add_new_item'               => __( 'Añadir Nuevo', 'wit' ),
		'edit_item'                  => __( 'Editar', 'wit' ),
		'update_item'                => __( 'Actualizar', 'wit' ),
		'view_item'                  => __( 'Ver', 'wit' ),
		'separate_items_with_commas' => __( 'Separar con comas', 'wit' ),
		'add_or_remove_items'        => __( 'Agregar o Eliminar', 'wit' ),
		'choose_from_most_used'      => __( 'Escoger de los más usados', 'wit' ),
		'popular_items'              => __( 'Populares', 'wit' ),
		'search_items'               => __( 'Buscar', 'wit' ),
		'not_found'                  => __( 'No Encontrado', 'wit' ),
		'no_terms'                   => __( 'No existen', 'wit' ),
		'items_list'                 => __( 'Lista Items', 'wit' ),
		'items_list_navigation'      => __( 'Navegación lista items', 'wit' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'tipo_solucion', array( 'soluciones', 'casos' ), $args );

}
add_action( 'init', 'custom_taxonomy_tipo_solucion', 0 );

// Register Custom Post Type Clientes
function custom_post_type_clientes() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'wit' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'wit' ),
		'menu_name'             => __( 'Clientes', 'wit' ),
		'name_admin_bar'        => __( 'Clientes', 'wit' ),
		'archives'              => __( 'Archivo', 'wit' ),
		'attributes'            => __( 'Atributos', 'wit' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'wit' ),
		'all_items'             => __( 'Todos', 'wit' ),
		'add_new_item'          => __( 'Añadir nuevo', 'wit' ),
		'add_new'               => __( 'Añadir nuevo', 'wit' ),
		'new_item'              => __( 'Nuevo', 'wit' ),
		'edit_item'             => __( 'Editar', 'wit' ),
		'update_item'           => __( 'Actualizar', 'wit' ),
		'view_item'             => __( 'Ver', 'wit' ),
		'view_items'            => __( 'Ver', 'wit' ),
		'search_items'          => __( 'Buscar', 'wit' ),
		'not_found'             => __( 'No encontrado', 'wit' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'wit' ),
		'featured_image'        => __( 'Imagen Destacada', 'wit' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'wit' ),
		'remove_featured_image' => __( 'Remove featured image', 'wit' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'wit' ),
		'insert_into_item'      => __( 'Insertar', 'wit' ),
		'uploaded_to_this_item' => __( 'Cargar', 'wit' ),
		'items_list'            => __( 'Lista de Items', 'wit' ),
		'items_list_navigation' => __( 'Items list navigation', 'wit' ),
		'filter_items_list'     => __( 'Filtrar lista', 'wit' ),
	);
	$args = array(
		'label'                 => __( 'Clientes', 'wit' ),
		'description'           => __( 'Clientes W-it', 'wit' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'			=> array( 'tipo_industria' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'clientes', $args );

}
add_action( 'init', 'custom_post_type_clientes', 0 );

// Register Custom Post Type casos
function custom_post_type_casos() {

	$labels = array(
		'name'                  => _x( 'Casos', 'Post Type General Name', 'wit' ),
		'singular_name'         => _x( 'Caso', 'Post Type Singular Name', 'wit' ),
		'menu_name'             => __( 'Casos', 'wit' ),
		'name_admin_bar'        => __( 'Casos', 'wit' ),
		'archives'              => __( 'Archivo', 'wit' ),
		'attributes'            => __( 'Atributos', 'wit' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'wit' ),
		'all_items'             => __( 'Todos', 'wit' ),
		'add_new_item'          => __( 'Añadir nuevo', 'wit' ),
		'add_new'               => __( 'Añadir nuevo', 'wit' ),
		'new_item'              => __( 'Nuevo', 'wit' ),
		'edit_item'             => __( 'Editar', 'wit' ),
		'update_item'           => __( 'Actualizar', 'wit' ),
		'view_item'             => __( 'Ver', 'wit' ),
		'view_items'            => __( 'Ver', 'wit' ),
		'search_items'          => __( 'Buscar', 'wit' ),
		'not_found'             => __( 'No encontrado', 'wit' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'wit' ),
		'featured_image'        => __( 'Imagen Destacada', 'wit' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'wit' ),
		'remove_featured_image' => __( 'Remove featured image', 'wit' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'wit' ),
		'insert_into_item'      => __( 'Insertar', 'wit' ),
		'uploaded_to_this_item' => __( 'Cargar', 'wit' ),
		'items_list'            => __( 'Lista de Items', 'wit' ),
		'items_list_navigation' => __( 'Items list navigation', 'wit' ),
		'filter_items_list'     => __( 'Filtrar lista', 'wit' ),
	);
	$args = array(
		'label'                 => __( 'Casos', 'wit' ),
		'description'           => __( 'Casos de Éxito W-it', 'wit' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'			=> array( 'tipo_industria', 'tipo_solucion', 'tipo_servicio' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-awards',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'casos', $args );

}
add_action( 'init', 'custom_post_type_casos', 0 );

// Register Custom Taxonomy Tipo Servicio
function custom_taxonomy_tipo_servicio() {

	$labels = array(
		'name'                       => _x( 'Servicios', 'Taxonomy General Name', 'wit' ),
		'singular_name'              => _x( 'Servicio', 'Taxonomy Singular Name', 'wit' ),
		'menu_name'                  => __( 'Servicios', 'wit' ),
		'all_items'                  => __( 'Todos', 'wit' ),
		'parent_item'                => __( 'Item Relacionado', 'wit' ),
		'parent_item_colon'          => __( 'Item relacionado:', 'wit' ),
		'new_item_name'              => __( 'Nombre Nuevo', 'wit' ),
		'add_new_item'               => __( 'Añadir Nuevo', 'wit' ),
		'edit_item'                  => __( 'Editar', 'wit' ),
		'update_item'                => __( 'Actualizar', 'wit' ),
		'view_item'                  => __( 'Ver', 'wit' ),
		'separate_items_with_commas' => __( 'Separar con comas', 'wit' ),
		'add_or_remove_items'        => __( 'Agregar o Eliminar', 'wit' ),
		'choose_from_most_used'      => __( 'Escoger de los más usados', 'wit' ),
		'popular_items'              => __( 'Populares', 'wit' ),
		'search_items'               => __( 'Buscar', 'wit' ),
		'not_found'                  => __( 'No Encontrado', 'wit' ),
		'no_terms'                   => __( 'No existen', 'wit' ),
		'items_list'                 => __( 'Lista Items', 'wit' ),
		'items_list_navigation'      => __( 'Navegación lista items', 'wit' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'tipo_servicio', array( 'casos' ), $args );

}
add_action( 'init', 'custom_taxonomy_tipo_servicio', 0 );

// Register Custom Taxonomy Tipo Industria
function custom_taxonomy_tipo_industria() {

	$labels = array(
		'name'                       => _x( 'Industrias', 'Taxonomy General Name', 'wit' ),
		'singular_name'              => _x( 'Industria', 'Taxonomy Singular Name', 'wit' ),
		'menu_name'                  => __( 'Industrias', 'wit' ),
		'all_items'                  => __( 'Todos', 'wit' ),
		'parent_item'                => __( 'Item Relacionado', 'wit' ),
		'parent_item_colon'          => __( 'Item relacionado:', 'wit' ),
		'new_item_name'              => __( 'Nombre Nuevo', 'wit' ),
		'add_new_item'               => __( 'Añadir Nuevo', 'wit' ),
		'edit_item'                  => __( 'Editar', 'wit' ),
		'update_item'                => __( 'Actualizar', 'wit' ),
		'view_item'                  => __( 'Ver', 'wit' ),
		'separate_items_with_commas' => __( 'Separar con comas', 'wit' ),
		'add_or_remove_items'        => __( 'Agregar o Eliminar', 'wit' ),
		'choose_from_most_used'      => __( 'Escoger de los más usados', 'wit' ),
		'popular_items'              => __( 'Populares', 'wit' ),
		'search_items'               => __( 'Buscar', 'wit' ),
		'not_found'                  => __( 'No Encontrado', 'wit' ),
		'no_terms'                   => __( 'No existen', 'wit' ),
		'items_list'                 => __( 'Lista Items', 'wit' ),
		'items_list_navigation'      => __( 'Navegación lista items', 'wit' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'tipo_industria', array( 'clientes', 'casos' ), $args );

}
add_action( 'init', 'custom_taxonomy_tipo_industria', 0 );

?>