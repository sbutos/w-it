$(document).ready(function () {
    //Le añado una clase al header después de 50px
    var lastScrollTop = 0;
    var delta = 50;
    var navbarHeight = $("header").height();

    $(window).scroll(function () {
        var st = $(this).scrollTop();
        if (Math.abs(lastScrollTop - st) <= delta) return;
        if (st > lastScrollTop && st > navbarHeight) {
            // Scroll Down
            $("header, .menu-trigger")
                .removeClass("nav-up")
                .css("transform", "translateY(-" + navbarHeight + "px)");
        } else {
            // Scroll Up
            if (st + $(window).height() < $(document).height()) {
                $("header, .menu-trigger")
                    .css("transform", "translateY(0px)")
                    .addClass("nav-up");
            }
        }

        lastScrollTop = st;
        if (st < 150) {
            $("header, .menu-trigger").addClass("nav-top");
        } else {
            $("header, .menu-trigger").removeClass("nav-top");
        }
    });

    //Menú responsive
    $("#hamburger").click(function () {
        if ($(".menu-responsive").css("right") != "0px") {
            $(this).addClass("close");
            $(".menu-responsive").addClass("opened");
            $("body").addClass("overflow-hidden");
        } else {
            $(this).removeClass("close");
            $("header a.logo").removeClass("menu-open");
            $(".menu-responsive").removeClass("opened");
            $("body").removeClass("overflow-hidden");
        }
    });
});
