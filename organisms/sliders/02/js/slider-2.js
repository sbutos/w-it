$(document).ready(function() {
	//Slider certificaciones
	$('.slider-2').slick({
		dots: true,
		arrows: true,
		infinite: true,
		speed: 1000,
		autoplay: false,
		slidesToShow: 4,
		slidesToScroll: 4,
		pauseOnHover: false,
		pauseOnFocus: false,
		responsive: [
		{
		  breakpoint: 851,
		  settings: {
		    slidesToShow: 3,
				slidesToScroll: 3,
				arrows: false
		  }
		},
		{
		  breakpoint: 641,
		  settings: {
		    slidesToShow: 2,
				slidesToScroll: 2,
				arrows: false
		  }
		},
		{
		  breakpoint: 451,
		  settings: {
		    slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false
		  }
		},
		]
	});
})
