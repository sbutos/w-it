		</main>
		<div id="form-footer" class="mask mask-top bg-gray container">
		    <div class="wrap-xl">
		        <div class="col-container">
		            <div class="col-40">
		                <?php
						$tituloFooter = get_field( 'titulo_form_footer', 'option' );
						$bajadaFooter = get_field( 'bajada_form_footer', 'option' );
						$telefonoSite = get_field( 'telefono_site', 'option' );
						$correoSite = get_field( 'correo_site', 'option' );
						$direccionSite = get_field( 'direccion_site', 'option' );
						?>
		                <?php if($tituloFooter) { ?>
		                <h3 class="title gdot"><?php echo $tituloFooter; ?></h3>
		                <?php } ?>
		                <?php if($bajadaFooter) { ?>
		                <p class="parrafo-m"><?php echo $bajadaFooter; ?></p>
		                <?php } ?>
		                <div class="contact-info-area">
		                    <?php if($telefonoSite) { ?>
		                    <div class="contact-info">
		                        <i class="icon-whatsapp"></i><span><?php echo $telefonoSite; ?></span>
		                    </div>
		                    <?php } ?>
		                    <?php if($correoSite) { ?>
		                    <div class="contact-info">
		                        <i class="icon-mail"></i><span><?php echo $correoSite; ?></span>
		                    </div>
		                    <?php } ?>
		                    <?php if($direccionSite) { ?>
		                    <div class="contact-info">
		                        <i class="icon-pin"></i><span><?php echo $direccionSite; ?></span>
		                    </div>
		                    <?php } ?>
		                </div>
		            </div>
		            <div class="col-55">
		                <div class="contact-form-area">
		                    <?php echo do_shortcode('[contact-form-7 id="169" title="Formulario de contacto"]', false); ?>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
		<div id="cta-rrss" class="container bg-dark-3">
		    <div class="col-45">
		        <div class="cta-content-area">
		            <?php $ctaText = get_field( 'cta_rrss', 'option' ); ?>
		            <?php if($ctaText) { ?>
		            <h5 class="gdot"><?php echo $ctaText; ?></h5>
		            <?php } ?>
		            <?php if ( have_rows( 'redes_sociales', 'option' ) ) : ?>
		            <div class="rrss-area">
		                <?php while ( have_rows( 'redes_sociales', 'option' ) ) : the_row(); ?>
		                <a href="<?php the_sub_field( 'url_rrss' ); ?>" target="_blank" class="rrss-link">
		                    <i class="icon-<?php the_sub_field( 'red_social' ); ?>"></i>
		                </a>
		                <?php endwhile; ?>
		            </div>
		            <?php endif; ?>
		        </div>
		    </div>
		</div>
		<?php if ( have_rows( 'logo_partner', 'option' ) ) : ?>
		<div id="partner-logos" class="bg-blue container">
		    <div class="wrap-xl">
		        <div class="partners-area">
		            <?php while ( have_rows( 'logo_partner', 'option' ) ) : the_row(); ?>
		            <?php $logo = get_sub_field( 'logo' ); ?>
		            <?php if ( $logo ) { ?>
		            <div class="logo-area">
		                <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
		            </div>
		            <?php } ?>
		            <?php endwhile; ?>
		        </div>
		    </div>
		</div>
		<footer>
		    <div class="wrap-xl">
		        <div class="footer-content">
		            <div class="brand-area">
		                <a href="<?php echo site_url('/'); ?>">
		                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo-wit.svg" alt="W-It">
		                </a>
		            </div>
		            <div class="menu-footer-area">
		                <ul id="main-menu" class="menu-foot">
		                    <?php wp_nav_menu(array('items_wrap' => '%3$s', 'theme_location'=>'Menu principal', 'container_class' => false, 'container' => false));?>
		                </ul>
		                <ul id="secondary-menu" class="menu-foot">
		                    <?php wp_nav_menu(array('items_wrap' => '%3$s', 'theme_location'=>'Menu secundario', 'container_class' => false, 'container' => false));?>
		                </ul>
		                <?php if($telefonoSite || $correoSite || $direccionSite) { ?>
		                <div class="contact-info-area">
		                    <?php if($telefonoSite) { ?>
		                    <div class="contact-info">
		                        <i class="icon-whatsapp"></i><span><?php echo $telefonoSite; ?></span>
		                    </div>
		                    <?php } ?>
		                    <?php if($correoSite) { ?>
		                    <div class="contact-info">
		                        <i class="icon-mail"></i><span><?php echo $correoSite; ?></span>
		                    </div>
		                    <?php } ?>
		                    <?php if($direccionSite) { ?>
		                    <div class="contact-info">
		                        <i class="icon-pin"></i><span><?php echo $direccionSite; ?></span>
		                    </div>
		                    <?php } ?>
		                </div>
		                <?php } ?>
		            </div>
		        </div>
		    </div>
		</footer>
		<?php endif; ?>
		<script>
AOS.init();
		</script>

		<?php wp_footer() ?>
		</body>

		</html>