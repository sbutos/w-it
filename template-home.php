<?php
/*
* Template Name: Inicio
*/
get_header();
?>
<section id="intro" class="contianer set-height">
    <div class="titulo-principal-area">
        <div class="wrap-xl">
            <div class="titulo-principal">
                <?php the_field('titulo_principal'); ?>
            </div>
            <?php $boton_bloque_1 = get_field( 'boton_bloque_1' ); ?>
            <?php if ( $boton_bloque_1 ) { ?>
            <a href="<?php echo $boton_bloque_1['url']; ?>" target="<?php echo $boton_bloque_1['target']; ?>"
                class="button is-rounded is-green"><?php echo $boton_bloque_1['title']; ?></a>
            <?php } ?>
        </div>
    </div>
    <?php $carousel_servicios = get_field( 'carousel_servicios' ); ?>
    <?php if ( $carousel_servicios ): ?>
    <div class="carousel-area">
        <div id="carousel-servicios">
            <?php foreach ( $carousel_servicios as $post ):  ?>
            <?php setup_postdata ( $post );
            $servicioThumb = get_the_post_thumbnail_url();
            $thumbID = get_post_thumbnail_ID();
            $servicioThumbAlt = get_post_meta ( $thumbID, '_wp_attachment_image_alt', true );
            ?>
            <div class="slide">
                <a href="<?php the_permalink(); ?>" class="service-box">
                    <div class="photo-bg-full cover" style="background-image: url(<?php echo $servicioThumb; ?>);"
                        title="<?php echo $servicioThumbAlt; ?>">
                        <div class="veil op-blue-60"></div>
                    </div>
                    <div class="content-box">
                        <h2><?php the_title(); ?></h2>
                    </div>
                </a>
            </div>
            <?php endforeach; ?>
            <?php wp_reset_postdata(); ?>
        </div>
    </div>
    <?php endif; ?>
</section>
<section class="container mask mask-bottom bg-gray spacer-3">
    <div class="wrap-xl">
        <div id="work-us">
            <div class="titulo">
                <?php the_field('titulo_bloque_2'); ?>
            </div>
            <?php if ( have_rows( 'cifras' ) ) : ?>
            <div class="cifras-area">
                <?php while ( have_rows( 'cifras' ) ) : the_row();
                $precifra = get_sub_field( 'pre_cifra' );
                $cifra = get_sub_field( 'cifra' );
                $postcifra = get_sub_field( 'post_cifra' );
                $descripcion = get_sub_field( 'descripcion' );
                ?>
                <div class="cifra-box">
                    <div class="cifra-item">
                        <div class="number-full">
                            <?php if($precifra) { ?>
                            <p class="precifra"><?php echo $precifra; ?></p>
                            <?php } ?>
                            <?php if($cifra) { ?>
                            <p class="cifra count"><?php echo $cifra; ?></p>
                            <?php } ?>
                            <?php if($postcifra) { ?>
                            <p class="postcifra"><?php echo $postcifra; ?></p>
                            <?php } ?>
                        </div>
                        <?php if($descripcion) { ?>
                        <div class="descripcion-area">
                            <p><?php echo $descripcion; ?></p>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<section class="container spacer-2">
    <div class="wrap-xl">
        <?php $tituloSolucionesHome = get_field( 'titulo_soluciones_home' ); ?>
        <?php if($tituloSolucionesHome) { ?>
        <div class="title-area">
            <h3 class="title gdot"><?php echo $tituloSolucionesHome; ?></h3>
        </div>
        <?php } ?>
        <div id="all-soluciones-home">
            <div class="content-area">
                <div class="col-25">
                    <div id="soluciones-filter" class="tabs-taxonomies-services">
                        <?php
                        $taxonomies = get_terms( array(
                            'taxonomy' => 'tipo_solucion',
                            'hide_empty' => true
                        ) );
                         
                        if ( !empty($taxonomies) ) :
                            foreach( $taxonomies as $category ) {
                                if( $category->parent == 0 ) {
                                    $output.= '<a class="button is-gray is-rounded has-icon" title="'. esc_attr( $category->name ) .'" href="#" data-tax="'. esc_attr( $category->slug ) .'">';
                                    $output.= '<span>'. esc_attr( $category->name ) .'</span><i class="icon-flecha-derecha-mediana"></i>';
                                    $output.='</a>';
                                }
                            }
                            echo $output;
                        endif;
                        ?>
                    </div>
                </div>
                <div class="col-70">
                    <div id="soluciones-posts">
                        <?php
                            if ( !empty($taxonomies) ) {
                                foreach( $taxonomies as $category ) {
                                    if( $category->parent == 0 ) { ?>
                        <div id="<?php echo esc_attr( $category->slug ); ?>" class="solucion-cat-container">
                            <?php
                            $argsSoluciones = array(
                                'post_type' => 'soluciones',
                                'posts_per_page' => -1,
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'tipo_solucion',
                                        'field' => 'slug',
                                        'terms' => $category->slug,
                                    )
                                )
                            );
                            $postsSoluciones = new WP_Query($argsSoluciones);
                            if( $postsSoluciones->have_posts() ){ ?>
                            <div class="icon-boxs-container">
                                <?php while( $postsSoluciones->have_posts() ) { $postsSoluciones->the_post(); ?>
                                <a href="<?php echo get_the_permalink(); ?>" class="icon-box-type-a">
                                    <?php $icono = get_field( 'icono' ); ?>
                                    <?php if ( $icono ) { ?>
                                    <img src="<?php echo $icono['url']; ?>" alt="<?php echo $icono['alt']; ?>"
                                        class="box-icon" />
                                    <?php } ?>
                                    <h3><?php  echo get_the_title(); ?></h3>
                                    <span class="go-icon"><i class="icon-flecha-derecha-mediana"></i></span>
                                </a>
                                <?php } wp_reset_postdata(); ?>
                            </div>
                            <?php } ?>
                        </div>
                        <?php
                                    }
                                }
                            } 
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="clientes-home" class="container spacer-2">
    <div class="wrap-xl">
        <div class="col-container v-centered">
            <div class="col-40">
                <?php $imagen_lateral_clientes_home = get_field( 'imagen_lateral_clientes_home' ); ?>
                <?php if ( $imagen_lateral_clientes_home ) { ?>
                <div class="img-full cover"
                    style="background-image: url(<?php echo $imagen_lateral_clientes_home['url']; ?>)"
                    title="<?php echo $imagen_lateral_clientes_home['alt']; ?>">
                </div>
                <?php } ?>
            </div>
            <div class="col-55 spacer-4">
                <?php $tituloClientesHome = get_field( 'titulo_clientes_home' ); ?>
                <?php if($tituloClientesHome) { ?>
                <div class="title-area">
                    <h3 class="title gdot"><?php echo $tituloClientesHome; ?></h3>
                </div>
                <?php } ?>
                <?php if ( have_rows( 'clientes' ) ) : ?>
                <div class="clientes-home-area">
                    <div id="clientes-carousel">
                        <?php while ( have_rows( 'clientes' ) ) : the_row(); ?>
                        <?php $logo_cliente = get_sub_field( 'logo_cliente' ); ?>
                        <?php if ( $logo_cliente ) { ?>
                        <div class="slide">
                            <div class="logo-container">
                                <img src="<?php echo $logo_cliente['url']; ?>"
                                    alt="<?php echo $logo_cliente['alt']; ?>" />
                            </div>
                        </div>
                        <?php } ?>
                        <?php endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<section class="container spacer-2">
    <div class="wrap-xl">
        <?php $tituloCasosExitoHome = get_field( 'titulo_casos_exito_home' ); ?>
        <?php if($tituloCasosExitoHome) { ?>
        <div class="title-area text-center">
            <h3 class="title gdot"><?php echo $tituloCasosExitoHome; ?></h3>
        </div>
        <?php } ?>
        <?php $casos_exito = get_field( 'casos_exito' ); ?>
        <?php if ( $casos_exito ) { ?>
        <div class="casos-exito-home">
            <div id="casos-home-slide">
                <?php foreach ( $casos_exito as $p ): ?>
                <div class="slide">
                    <div class="caso-content">
                        <div class="col-container">
                            <?php
                                $casoThumb = get_the_post_thumbnail_url($p);
                                $casoThumbID = get_post_thumbnail_ID($p);
                                $casoThumbAlt = get_post_meta ( $casoThumbID, '_wp_attachment_image_alt', true );
                            ?>
                            <div class="col-55">
                                <div class="img-full cover" style="background-image: url(<?php echo $casoThumb; ?>)"
                                    title="<?php echo $casoThumbAlt; ?>"></div>
                            </div>
                            <div class="col-45">
                                <?php
                                $rel_cliente = get_field( 'rel_cliente', $p );
                                $idC = $rel_cliente->ID;
                                $logo = get_field( 'logo', $idC );
                                $encabezadoPrincipal = get_field( 'encabezado_principal', $p );
                                $introduccion = get_field('introduccion', $p);
                                $terms = get_the_terms( $p, 'tipo_industria' );
                                if ( !empty( $terms ) ){
                                    
                                    $term = array_shift( $terms );
                                    $industriaID = $term->term_id;
                                    $industriaName = $term->name;

                                    $taxonomy_prefix = 'tipo_industria';
                                    $term_id = $industriaID;
                                    $term_id_prefixed = $taxonomy_prefix .'_'. $term_id;
                                    $icono = get_field( 'icono', $term_id_prefixed ); } ?>
                                <div class="caso-info">
                                    <div class="caso-industria">
                                        <?php if ( $icono ) { ?>
                                        <img src="<?php echo $icono['url']; ?>" alt="<?php echo $icono['alt']; ?>" />
                                        <span><?php echo $industriaName; ?></span>
                                        <?php } ?>
                                    </div>
                                    <div class="caso-cliente">
                                        <?php if ( $logo ) { ?>
                                        <div class="cliente-logo">
                                            <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
                                        </div>
                                        <?php } ?>
                                        <div class="caso-data">
                                            <h3 class="title-2 gdot"><?php echo $encabezadoPrincipal; ?></h3>
                                            <p class="parrafo-l"><?php echo $introduccion; ?></p>
                                        </div>
                                    </div>
                                    <div class="navigation-area">
                                        <div class="slide-arrows">
                                            <a href="#" class="arrow prev button is-gray"><i
                                                    class="icon-flecha-izquierda-mediana"></i></a>
                                            <a href="#" class="arrow next button is-gray"><i
                                                    class="icon-flecha-derecha-mediana"></i></a>
                                        </div>
                                        <a href="<?php echo get_permalink( $p ); ?>"
                                            class="button is-green is-rounded"><?php _e('Leer más', 'wit'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php } ?>
    </div>
</section>
<section class="container spacer-3">
    <div class="wrap-xl">
        <?php $tituloIndustriasHome = get_field( 'titulo_industrias_home' ); ?>
        <?php if($tituloIndustriasHome) { ?>
        <div class="title-area">
            <h3 class="title gdot"><?php echo $tituloIndustriasHome; ?></h3>
        </div>
        <?php } ?>
        <div class="industrias-home">
            <?php
            $industriasTax = get_terms( array(
                'taxonomy' => array('tipo_industria'),
                'hide_empty' => false
            ) );
                
            if ( !empty($industriasTax) ) : ?>
            <div class="icon-boxs-container">
                <?php
                foreach( $industriasTax as $industria) {
                    if( $industria->parent == 0 ) {
                        $industria_prefix = 'tipo_industria';
                        $industria_id = $industria->term_id;
                        $industria_prefixed = $industria_prefix .'_'. $industria_id;
                        $iconoIndustria = get_field( 'icono', $industria_prefixed );
                        $industriaName = $industria->name;
                        $industriaSlug = $industria->slug; ?>
                <a href="#<?php echo $industriaSlug; ?>" class="icon-box-type-a">
                    <?php if ( $iconoIndustria ) { ?>
                    <img src="<?php echo $iconoIndustria['url']; ?>" alt="<?php echo $iconoIndustria['alt']; ?>"
                        class="box-icon" />
                    <?php } ?>
                    <h3><?php  echo $industriaName; ?></h3>
                    <span class="go-icon"><i class="icon-flecha-derecha-mediana"></i></span>
                </a>
                <?php
                    }
                } ?>
            </div>
            <?php
            endif;
            ?>
        </div>
    </div>
</section>
<section class="container spacer-4">
    <div id="nosotros-home">
        <?php $imagen_nosotros_home = get_field( 'imagen_nosotros_home' ); ?>
        <?php if ( $imagen_nosotros_home ) { ?>
        <div class="img-full cover mask mask-side"
            style="background-image: url(<?php echo $imagen_nosotros_home['url']; ?>)"
            title="<?php echo $imagen_nosotros_home['alt']; ?>">
        </div>
        <?php } ?>
        <div class="wrap-xl">
            <div class="col-45">
                <div class="text-content-area spacer-5">
                    <?php
                    $tituloNosotrosHome = get_field( 'titulo_nosotros_home' );
                    $bajadaNosotrosHome = get_field( 'bajada_nosotros_home' );
                    $link_nosotros_home = get_field( 'link_nosotros_home' );
                    ?>
                    <?php if($tituloNosotrosHome) { ?>
                    <h3 class="title gdot"><?php echo $tituloNosotrosHome; ?></h3>
                    <?php } ?>
                    <?php if($bajadaNosotrosHome) { ?>
                    <p class="parrafo-l spacer-2">
                        <?php echo $bajadaNosotrosHome; ?>
                    </p>
                    <?php } ?>
                    <?php if ( $link_nosotros_home ) { ?>
                    <a href="<?php echo $link_nosotros_home['url']; ?>"
                        target="<?php echo $link_nosotros_home['target']; ?>"
                        class="button is-green is-rounded"><?php echo $link_nosotros_home['title']; ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="novedades-home" class="container spacer-2">
    <div class="wrap-xl">
        <div class="content">
            <?php $tituloNovedadesHome = get_field( 'titulo_novedades_home' ); ?>
            <?php if($tituloNovedadesHome) { ?>
            <h3 class="title gdot"><?php echo $tituloNovedadesHome; ?></h3>
            <?php } ?>
            <?php $link_novedades = get_field( 'link_novedades' ); ?>
            <?php if ( $link_novedades ) { ?>
            <a href="<?php echo $link_novedades['url']; ?>" target="<?php echo $link_novedades['target']; ?>"
                class="button is-gray is-rounded has-icon"><span><?php echo $link_novedades['title']; ?></span><i
                    class="icon-flecha-derecha-mediana"></i></a>
            <?php } ?>
        </div>
    </div>
    <div class="novedades-area">
        <?php
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'post' ),
            'posts_per_page'         => -1,
            'order'                  => 'DESC',
            'orderby'                => 'date',
        );

        // The Query
        $novedades = new WP_Query( $args );

        // The Loop
        if ( $novedades->have_posts() ) { ?>
        <div id="carousel-novedades">
            <?php while ( $novedades->have_posts() ) { $novedades->the_post();
                $newsThumb = get_the_post_thumbnail_url();
                $newsThumbID = get_post_thumbnail_ID();
                $newsThumbAlt = get_post_meta ( $newsThumbID, '_wp_attachment_image_alt', true );    
            ?>
            <div class="slide">
                <a href="<?php the_permalink(); ?>" class="news-box">
                    <div class="img-full cover" style="background-image: url(<?php echo $newsThumb; ?>);"
                        title="<?php echo $newsThumbAlt; ?>">
                        <div class="veil gradient-back"></div>
                        <div class="veil gradient-front"></div>
                    </div>
                    <div class="news-content">
                        <h4><?php the_title(); ?></h4>
                        <span class="button is-green is-rounded has-icon"><span>Leer Más</span><i
                                class="icon-flecha-derecha-mediana"></i></span>
                    </div>
                </a>
            </div>
            <?php } ?>
        </div>
        <?php }
        wp_reset_postdata();
        ?>
    </div>
    <div class="wrap-xl">
        <div class="slide-arrows">
            <a href="#" class="arrow prev button is-green"><i class="icon-flecha-izquierda-mediana"></i></a>
            <a href="#" class="arrow next button is-green"><i class="icon-flecha-derecha-mediana"></i></a>
        </div>
    </div>
</section>
<script>
$(document).ready(function() {
    $('header').addClass('bg-white');
});
</script>
<?php get_footer(); ?>