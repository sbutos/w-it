var slider5 = null;
$(document).ready(function() {
	slider5 = $('.slider-5').slick({
		slidesToShow: 1,
	  slidesToScroll: 1,
	  fade: false,
		speed: 700,
		arrows: true,
		dots: true,
		responsive: [
		{
		  breakpoint: 851,
		  settings: {
				arrows: false,
		  }
		}
		]
	});
	equalOuterHeight($(".slider-5 .slide .content"));
})
